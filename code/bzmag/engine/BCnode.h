#ifndef BZMAG_ENGINE_BCNODE_H
#define BZMAG_ENGINE_BCNODE_H

/*
Description : Boundary condition Node
Last Update : 2020.07.22
Author : Jaenam Bae (jaenam@dongyang.ac.kr)
*/

#include "GeometricEntity.h"
#include "core/node.h"

namespace bzmag
{
namespace engine
{
	class Expression;
    class BCNode : public Node
    {
	public:
		enum BCTYPE {
			UNDEFINED = -1,	// 정의되지 않음
			NEUMANN,		// 자연경계(Natural BC)
			DIRICHLET,		// 고정경계(Fixed BC)
			PERIODIC		// 주기경계(Periodic BC)
		};

		typedef std::list<X_monotone_curve_2> Curves;

    public:
		BCNode();
        virtual ~BCNode();
        DECLARE_CLASS(BCNode, Node);

    public:
		// 경계조건 설정
		void setBCType(const BCTYPE& type);
		const BCTYPE& getBCType() const;
		virtual bool setBCValue(const String& value);

		// 경계(커브) 추가 ; 세그멘테이션 되기 이전의 커브가 추가되어야 함
		void addBoundary(const X_monotone_curve_2& curve);
		
		// 주어진 커브가 경계 커브에 포함되는지 테스트 ; 세그멘테이션 이전의 커브로 테스트 해야 함
		bool testCurve(const X_monotone_curve_2& test_curve) const;

		// 시작점과 끝점의 직선으로 구성된 세그먼트가 경계에 포함되는지 테스트
		bool testSegment(const Traits_2::Point_2& v1, const Traits_2::Point_2& v2) const;

		// 클리어
		void clear();

		int32 getNumberOfCurves() const;
		const Curves& getCurves();

    public:
        virtual bool update();
        virtual void onAttachTo(Node* parent);
        virtual void onDetachFrom(Node* parent);

    public:
        static void bindMethod();
        static void bindProperty();
		static bool testVertex(const Traits_2::Point_2& pt, const X_monotone_curve_2& test_curve);
    
    protected:
		BCTYPE type_;
		Ref<Expression> value_;

		// 경계조건 커브
		Curves curves_;

		static float64 tol_;
    };

#include "BCnode.inl"

}
}

#endif //BZMAG_ENGINE_BCNODE_H