#ifndef BZMAG_ENGINE_GEOMETRYTOTRIANGLE_H
#define BZMAG_ENGINE_GEOMETRYTOTRIANGLE_H


#include "GeometricEntity.h"
#include "PolyHelper.h"
#include "BCNode.h"
#include "core/node.h"
#include "core/singleton3.h"
#include "core/tuple2.h"
#include <CGAL/Arr_simple_point_location.h>
#include <CGAL/Arr_batched_point_location.h>
#include <functional>

namespace bzmag
{
namespace engine
{
class GeometryToTriangle : public Node, public Singleton3<GeometryToTriangle>
{
public:

	typedef std::pair<int32, Polygon_set_2> Polyset;
	typedef std::list<Polyset>              PolySets;
	typedef std::list<PolyHelper>           PolyUnits;
	typedef std::list<Traits_2::Point_2>    Points;

	typedef Tuple2<float64> Vert;
	typedef std::vector<Vert> Vertices;

	typedef std::pair<int32, int32> Seg;
	typedef std::vector<Seg> Segments;

	typedef std::pair<int32, Vert> Reg;
	typedef std::vector<Reg> Regions;

	class FindPolyHelper : public std::binary_function<PolyHelper, int, bool>
	{
	public:
		bool operator() (const PolyHelper& p, int32 b) const
		{
			return (p.ID() == b);
		}
	};

	typedef std::list<const BCNode*> BCNodes;

public:
	GeometryToTriangle();
	virtual ~GeometryToTriangle();
	DECLARE_CLASS(GeometryToTriangle, Node);

public:
	bool makePolyStructures(const String& path);
	void linkBoundaryConditions(const String& path);

	void setBasedNumberOfElements(int32 num_e);
	int32 getBasedNumberOfElements() const;

	void setBasedAngle(float64 angle);
	float64 getBasedAngle() const;

	void setTolerance(float64 angle);
	float64 getTolerance() const;
	

	PolyHelper* findPolyHelper(int32 ID);
	float64 area(int32 ID);
	float64 getMaximumAreaOfElements(int32 ID);
	void clear();

	Vertices::iterator firstVertex();
	Vertices::iterator lastVertex();

	Segments::iterator firstSegment();
	Segments::iterator lastSegment();

	std::vector<int64>::iterator firstSegmentMarker();
	std::vector<int64>::iterator lastSegmentMarker();

	Regions::iterator  firstRegion();
	Regions::iterator  lastRegion();

	Points::iterator   firstHole();
	Points::iterator   lastHole();


protected:
	void set_root_node(const Node* node);
	void set_root_BC_node(const Node* node);
	bool valid();
	bool valid(Polyset& polyset);
	float64 calculate_based_segment_length(int32 num_elements = based_elements_num_);
	bool config_zorder(Polyset& polyset);

	// 서로겹치지 않게 영역분할 후 세그멘테이션 한다
	// 기저 길이는 mm, 각도는 deg단위이다
	void make_zorder_polyset(float64 based_length, float64 based_angle = 3.0);
	void extract_points(Polygon_set_2& polyset);
	void extract_based_points_from_polyholes(Polygon_with_holes_2& polyhole);
	void extract_based_points_from_polygon(const Polygon_2& polygon);
	void make_triangle_input(Polygon_2& polygon, int32 ID, Traits_2::Point_2 pt);
	void approximate_vertices();
	void erase_duplicated_points();
	Vertices::const_iterator find_vertex(const Vert& v, const Vertices& vertices);
	void erase_duplicated_segments();
	Segments::const_iterator find_segment(const Seg& s, const Segments& segments);
	void find_holes(float64 based_length, float64 based_angle = 10);

	const BCNode* test_segment_on_BC(const Vert& ss, const Vert& tt);

public:
	static void bindMethod();
	static void bindProperty();

private:
	// 주어진 폴리셋트 (ID, Polygon_set_2의 조합)
	PolySets  polysets_;

	// z-order를 고려한 재배치 (서로 겹치는 폴리셋이 존재하지 않게 만듦)
	PolySets  zorder_polysets_;

	// PolyHelper : Polygon_set_2을 Triangle의 입력으로 만들수 있게 바꿔주는 핼퍼의 집합
	PolyUnits poly_units_;

	// 기저절점
	Points points_;

	// 세그멘티드 도메인 집합(모든 폴리셋의 집합)
	Polygon_set_2 whole_domain_;

	// Head Node(ID) 에 대한 요구 요소갯수
	std::map<int32, int32> required_elements_;

	// 경계조건
	BCNodes bc_nodes_;

	// ----------------------
	// Triangle Input 데이터
	Vertices vertices_;
	std::vector<int64> vertices_maker_;
	Segments segments_;
	std::vector<int64> segment_makers_;
	Regions  regions_;
	Points   holes_;


	// 최소 요소 생성 갯수?
	static int32 based_elements_num_;
	static float64 based_angle_;

	// 동일점 인정오차 0.001mm
	static float64 tol_;
};

#include "GeometryToTriangle.inl"
}
}

#endif //BZMAG_ENGINE_GEOMETRYTOTRIANGLE_H