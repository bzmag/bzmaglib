#include "BCnode.h"
#include "Expression.h"

#include "core/tuple2.h"
#include "core/simplepropertybinder.h"
#include "core/enumpropertybinder.h"
#include "core/nodeeventpublisher.h"
#include <CGAL/Polygon_2.h>

using namespace bzmag;
using namespace bzmag::engine;

IMPLEMENT_CLASS(BCNode, Node);

// 동일점 인정 오차
float64 BCNode::tol_ = 1e-9;

//----------------------------------------------------------------------------
BCNode::BCNode() : type_(UNDEFINED)
{
	uint32 key = getID();
	value_ = new Expression();
	value_->setKey("bc_" + std::to_string(key));
}

//----------------------------------------------------------------------------
BCNode::~BCNode()
{

}

//----------------------------------------------------------------------------
void BCNode::clear()
{
	type_ = UNDEFINED;
	curves_.clear();
}


//----------------------------------------------------------------------------
bool BCNode::setBCValue(const String& value)
{
	if (!value_->setExpression(value))
		return false;
	return true;
}

//----------------------------------------------------------------------------
void BCNode::addBoundary(const X_monotone_curve_2& curve)
{
	curves_.push_back(curve);
}

//----------------------------------------------------------------------------
// 세그멘테이션 되기전의 커브(General Polygon을 이루는 커브)에 대한 테스트
bool BCNode::testCurve(const X_monotone_curve_2& test_curve) const
{
	Traits_2::Point_2 ss = test_curve.source();
	Traits_2::Point_2 tt = test_curve.target();

	Curves::const_iterator it;
	for (it = curves_.cbegin(); it != curves_.cend(); ++it) {
		const X_monotone_curve_2& curve = *it;
		if (testVertex(ss, curve) && testVertex(tt, curve))
		{
			// X_monoton_curve_2는 최대 180도까지 표현 가능한 커브(0~180도)
			// 따라서 임의 커브의 시작점과 끝점이 경계 커브 위에 존재한다면
			// 무조건 경계커브내에 임의 커브가 존재하는 것임
			return true;
			/*
			if (curve.is_circular() && test_curve.is_circular()) {
				if (curve.orientation() == test_curve.orientation()) {
					return true;
				}
			}
			if (curve.is_linear() && test_curve.is_linear()) {
				return true;
			}
			*/
		}
	}
	return false;
}

//----------------------------------------------------------------------------
// 세그멘테이션 후의 직선에 대한 테스트
bool BCNode::testSegment(const Traits_2::Point_2& v1, const Traits_2::Point_2& v2) const
{
	Curves::const_iterator it;
	for (it = curves_.cbegin(); it != curves_.cend(); ++it) {
		const X_monotone_curve_2& curve = *it;
		if (testVertex(v1, curve) && testVertex(v2, curve))
		{
			return true;
		}
	}

	return false;
}

//----------------------------------------------------------------------------
bool BCNode::testVertex(const Traits_2::Point_2& pt, const X_monotone_curve_2& curve)
{
	typedef CGAL::Point_2<K>                 Pt2;
	typedef CGAL::Circle_2<K>                Circ2;
	typedef CGAL::Line_arc_2<K>              LineArc2;
	typedef CGAL::Circular_arc_2<K>          CircleArc2;
	typedef CGAL::CK2_Intersection_traits<K, Circ2, LineArc2>::type InterResLine;
	typedef CGAL::CK2_Intersection_traits<K, Circ2, CircleArc2>::type InterResCircle;
	typedef Tuple2<float64>                  Vertex2;

	Traits_2::Point_2 source = curve.source();
	Traits_2::Point_2 target = curve.target();

	Vertex2 v_pt(CGAL::to_double(pt.x()), CGAL::to_double(pt.y()));
	Vertex2 v_ss(CGAL::to_double(source.x()), CGAL::to_double(source.y()));
	Vertex2 v_tt(CGAL::to_double(target.x()), CGAL::to_double(target.y()));

	// 커브의 시작점 끝점 동일 시 실패 --> 쓰래기커브라고 판단
	float64 diff_x = (v_ss.x_ - v_tt.x_);
	float64 diff_y = (v_ss.y_ - v_tt.y_);
	float64 torr = std::sqrt(diff_x*diff_x + diff_y * diff_y);
	if (torr < tol_) {
		return false;
	}
	/*
	// source 및 target와 pt 사이의 거리
	Vertex2 d_st(v_ss.x_ - v_pt.x_, v_ss.y_ - v_pt.y_);
	Vertex2 d_tt(v_tt.x_ - v_pt.x_, v_tt.y_ - v_pt.y_);

	float64 dist_st = d_st.x_ * d_st.x_ + d_st.y_ * d_st.y_;
	float64 dist_tt = d_tt.x_ * d_tt.x_ + d_tt.y_ * d_tt.y_;
	dist_st = sqrt(dist_st);
	dist_tt = sqrt(dist_tt);

	// pt가 curve의 시작점 혹은 끝점과 오차 범위 내에 있다면
	// on point로 인정하지 않음
	if ((dist_st < tol_) || (dist_tt < tol_)) {
		return false;
	}
	*/

	// 오차범위 인정을 위한 포인트에 해당하는 원
	Circ2 c(Pt2(v_pt.x_, v_pt.y_), tol_*tol_);

	// curve의 시작점 끝점
	Pt2 ss(v_ss.x_, v_ss.y_);
	Pt2 tt(v_tt.x_, v_tt.y_);

	if (curve.is_linear())
	{
		std::vector<InterResLine> output;
		LineArc2 l(ss, tt);
		CGAL::intersection(c, l, std::back_inserter(output));

		return (output.size() > 0) ? true : false;
	}
	else
	{
		// 커브의 Supporting Circle 추출
		Circle_2 circle = curve.supporting_circle();

		// Supporting Circle의 반지름, 중심점 추출
		float64 squared_radii = CGAL::to_double(circle.squared_radius());
		float64 radii = std::sqrt(squared_radii);
		Point_2 org = circle.center();
		float64 ox = CGAL::to_double(org.x());
		float64 oy = CGAL::to_double(org.y());

		// 커브의 중심과 커브의 시작점간 좌표를 이용한 
		// 커브(아크)의 시작점 각도 계산
		float64 vsx = v_ss.x_ - ox;
		float64 vsy = v_ss.y_ - oy;
		float64 vs_ang = std::atan2(vsy, vsx);

		// 커브의 중심점과 커브의 끝점간 좌표를 이용한
		// 커브(아크)의 끝점 각도 계산
		float64 vtx = v_tt.x_ - ox;
		float64 vty = v_tt.y_ - oy;
		float64 vt_ang = std::atan2(vty, vtx);

		// 커브의 각도 계산
		float64 d_ang = (vt_ang - vs_ang);

		// 커브 중심점에서의 각도 계산
		if (curve.orientation() == CGAL::COUNTERCLOCKWISE)
		{
			if (d_ang < 0) d_ang += (2 * CGAL_PI);
		}
		else
		{
			if (d_ang > 0) d_ang -= (2 * CGAL_PI);
		}
		float64 angle = vs_ang + d_ang * 0.5;

		// 커브 중심점의 좌표계산
		float64 mx = ox + radii * cos(angle);
		float64 my = oy + radii * sin(angle);
		Pt2 mm(mx, my);

		// 세점을 이용한 아크 만들기
		if (curve.orientation() != CGAL::COUNTERCLOCKWISE) {
			std::swap(ss, tt);
		}
		CircleArc2 a(ss, mm, tt);


		// 아래 intersection 함수가 예전 코딩시(2016.05.25)에는 두번째 줄이 에러없이 컴파일 되었고
		// 지금(2016.07.25)은 첫번째 줄이 에러없이 컴파일 됨
		// 조금 이상하니 주의!!
		std::vector<InterResCircle> output;
		CGAL::intersection(a, c, std::back_inserter(output));

		return (output.size() > 0) ? true : false;
	}
}

//----------------------------------------------------------------------------
const BCNode::Curves& BCNode::getCurves()
{
	return curves_;
}

//----------------------------------------------------------------------------
bool BCNode::update()
{
    return true;
}

//----------------------------------------------------------------------------
void BCNode::onAttachTo(Node* parent)
{

}


//----------------------------------------------------------------------------
void BCNode::onDetachFrom(Node* parent)
{

}

//----------------------------------------------------------------------------
void BCNode::bindProperty()
{
	// enum
	BIND_ENUM_PROPERTY(BCTYPE, BC_Type, &setBCType, &getBCType);
		ENUM_PROPERTY_ADD(BCTYPE, UNDEFINED);
		ENUM_PROPERTY_ADD(BCTYPE, NEUMANN);
		ENUM_PROPERTY_ADD(BCTYPE, DIRICHLET);
		ENUM_PROPERTY_ADD(BCTYPE, PERIODIC);

	BIND_PROPERTY(int32, NumberOfCurves, 0, &getNumberOfCurves);
}

