#ifndef BZMAG_ENGINE_GEOMROTATENODE_H
#define BZMAG_ENGINE_GEOMROTATENODE_H

/*
Description : Rotate Nodes
Last Update : 2016.04.20
Author : Jaenam Bae (jaenam@dongyang.ac.kr)
*/

#include "GeomBaseNode.h"
//#include "expression.h"


namespace bzmag
{
namespace engine
{
    class Expression;
    class GeomRotateNode : public GeomBaseNode
    {
    public:
        GeomRotateNode();
        virtual ~GeomRotateNode();
        DECLARE_CLASS(GeomRotateNode, GeomBaseNode);

    public:
        bool setParameters(const String& angle);
        void setAngle(const String& angle);

        float64 getAngle() const;
        const String& getAngleAsString() const;

    public:
		// 이하 재정의 되어야 함
		virtual Transformation getMyTransform();
		virtual String description() const;

	protected:
		virtual bool make_geometry(Polygon_set_2& polyset, Curves& curves, Vertices& vertices, Transformation transform);

    public:
        static void bindMethod();
        static void bindProperty();

    private:
        Ref<Expression> angle_;
        String sangle_;
    };

#include "GeomRotateNode.inl"

}
}

#endif //BZMAG_ENGINE_GEOMROTATENODE_H