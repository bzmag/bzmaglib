#include "CoilNode.h"
#include "core/methodbinder.h"

using namespace bzmag;
using namespace bzmag::engine;

//----------------------------------------------------------------------------
static void CoilNode_v_setReferenceNode_n(CoilNode* self, Parameter* param)
{
	GeomHeadNode* head = param->in()->get<GeomHeadNode*>(0).get();
	self->setReferenceNode(head);
}

//----------------------------------------------------------------------------
static void CoilNode_n_getReferenceNode_v(CoilNode* self, Parameter* param)
{
	GeomHeadNode* head = self->getReferenceNode();
	param->out()->get<GeomHeadNode*>(0) = head;
}


//----------------------------------------------------------------------------
void CoilNode::bindMethod()
{
	BIND_METHOD(v_setReferenceNode_n, CoilNode_v_setReferenceNode_n);
	BIND_METHOD(n_getReferenceNode_v, CoilNode_n_getReferenceNode_v);
}
