
//----------------------------------------------------------------------------
inline void WindingNode::setCurrent(float64 I)
{
	I_ = I;
}

//----------------------------------------------------------------------------
inline float64 WindingNode::getCurrent() const
{
	return I_;
}

//----------------------------------------------------------------------------
inline void WindingNode::setNumberOfParallelBranches(int32 a)
{
	a_ = a;
}

inline int32 WindingNode::getNumberOfParallelBranches() const
{
	return a_;
}
