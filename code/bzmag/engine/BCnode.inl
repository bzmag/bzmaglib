
//-----------------------------------------------------------------------------
inline const BCNode::BCTYPE& BCNode::getBCType() const
{
	return type_;
}

//-----------------------------------------------------------------------------
inline void BCNode::setBCType(const BCTYPE& type)
{
	type_ = type;
}

//-----------------------------------------------------------------------------
inline int32 BCNode::getNumberOfCurves() const
{
	return (int32)(curves_.size());
}