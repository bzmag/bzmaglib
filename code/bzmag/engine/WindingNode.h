#ifndef BZMAG_ENGINE_WINDINGNODE_H
#define BZMAG_ENGINE_WINDINGNODE_H

/*
Description : Winding Node for Handling a Windings
Last Update : 2020.11.04
Author : Jaenam Bae (jaenam@dongyang.ac.kr)
*/

#include "CoilNode.h"
#include "core/node.h"
#include "core/vector2.h"

namespace bzmag
{
namespace engine
{
    class WindingNode : public Node
    {
	public:
		typedef std::list<CoilNode*> Coils;

    public:
		WindingNode();
        virtual ~WindingNode();
        DECLARE_CLASS(WindingNode, Node);

	public:
		void setCurrent(float64 I);
		float64 getCurrent() const;

		void setNumberOfParallelBranches(int32 a);
		int32 getNumberOfParallelBranches() const;
		void clear();

		void getRelatedHeadNode(std::list<GeomHeadNode*>& list);


    public:
        virtual bool update();
        virtual void onAttachTo(Node* parent);
        virtual void onDetachFrom(Node* parent);

    public:
        static void bindMethod();
        static void bindProperty();
    


    protected:
		int32 a_;		// 병렬회로수
		float64 I_;		// 전류밀도
    };

#include "windingnode.inl"

}
}

#endif //BZMAG_ENGINE_WINDINGNODE_H