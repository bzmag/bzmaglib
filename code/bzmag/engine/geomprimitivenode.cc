#include "GeomPrimitiveNode.h"
#include "GeomHeadNode.h"
#include "CSNode.h"

using namespace bzmag;
using namespace bzmag::engine;

IMPLEMENT_ABSTRACTCLASS(GeomPrimitiveNode, GeomBaseNode);

//----------------------------------------------------------------------------
GeomPrimitiveNode::GeomPrimitiveNode()
{

}

//----------------------------------------------------------------------------
GeomPrimitiveNode::~GeomPrimitiveNode()
{

}

//----------------------------------------------------------------------------
Transformation GeomPrimitiveNode::getMyTransform()
{
	Transformation trans;
	if (cs_) {
		trans = cs_->transformation();
	}
	return trans;
}

//----------------------------------------------------------------------------
void GeomPrimitiveNode::updateTransform()
{
	// 변환 업데이트
	Transformation trans = getMyTransform();
	last_trans_ = trans;
}

//----------------------------------------------------------------------------
void GeomPrimitiveNode::updateLinkedNode()
{
	Transformation trans = getMyTransform();
	LinkedHeads::iterator it;
	for (it = linked_heads_.begin(); it != linked_heads_.end(); ++it)
	{
		(*it).second = trans;
	}
}