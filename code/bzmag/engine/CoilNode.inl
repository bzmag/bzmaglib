

//-----------------------------------------------------------------------------
inline bool CoilNode::getDirection() const
{
	return direction_;
}

//-----------------------------------------------------------------------------
inline void CoilNode::setDirection(bool dir)
{
	direction_ = dir;
}

//-----------------------------------------------------------------------------
inline int32 CoilNode::getNumberOfTurns() const
{
	return turns_;
}

//-----------------------------------------------------------------------------
inline void CoilNode::setNumberOfTurns(const int32 turns)
{
	turns_ = turns;
}
