#ifndef BZMAG_ENGINE_GEOMPRIMITIVENODE_H
#define BZMAG_ENGINE_GEOMPRIMITIVENODE_H

/*
Description : Abstract Node for Primitive Operation
Primitive 노드는 최초의 형상을 만들어 내는 노드들의 집합이다.
ex) 원, 사각형, (곡)선, 복제
Last Update : 2020.08.13
Author : Jaenam Bae (jaenam@dongyang.ac.kr)
*/

#include "GeomBaseNode.h"

namespace bzmag
{
namespace engine
{
    class GeomPrimitiveNode : public GeomBaseNode
    {
    public:
        typedef std::list<Ref<GeomHeadNode>> ToolNodes;
        typedef ToolNodes::iterator ToolIter;

    public:
		GeomPrimitiveNode();
        virtual ~GeomPrimitiveNode();
        DECLARE_ABSTRACTCLASS(GeomPrimitiveNode, GeomBaseNode);

    public:
		// 이하 재정의 되어야 함
		virtual Transformation getMyTransform();

	protected:
		virtual void updateTransform();
		virtual void updateLinkedNode();
    };
}
}

#endif //BZMAG_ENGINE_GEOMPRIMITIVENODE_H