#ifndef BZMAG_ENGINE_SIMPLEGEOMETRY_H
#define BZMAG_ENGINE_SIMPLEGEOMETRY_H

#include "GeometricEntity.h"
#include "BCNode.h"
#include "core/primitivetype.h"
#include <CGAL/Arr_simple_point_location.h>
#include <CGAL/Arr_batched_point_location.h>

namespace bzmag
{
namespace engine
{

// 하나의 도메인 (GeomHeadNode의 Polyset)을 서브도메인으로 나누어 주는 클래스
// 전체 도메인 (모든 HeadNode 의 Polyset 들의 집합) 기저절점에 대해 커브를 쪼개어 세그멘테이션의 사전준비를 하고,
// 경계조건을 고려한 세그멘테이션까지 담당하고 있음
class SimpleGeometry
{
public:
	typedef std::vector<X_monotone_curve_2>               Curves;
	typedef std::pair<Polygon_2, int32>                   Polygon_tag;
	typedef std::vector<Polygon_tag>                      Polygons;
	typedef CGAL::Arr_simple_point_location<Arrangement>  Point_location;
	typedef CGAL::Arr_point_location_result<Arrangement>  Point_location_result;
	typedef Arrangement::Halfedge_const_handle            Halfedge_const_handle;
	typedef Arrangement::Halfedge_handle                  Halfedge_handle;
	typedef Arrangement::Vertex_handle                    Vertex_handle;
	typedef Arrangement::Vertex_const_handle              Vertex_const_handle;
	typedef Arrangement::Face_const_handle                Face_const_handle;
	typedef std::pair<Traits_2::Point_2, Point_location_result::Type>   Query_result;

public:
	SimpleGeometry();
	virtual ~SimpleGeometry();
	void setPolyset(Polygon_set_2& geometry);
	bool insertPoints(std::list<Traits_2::Point_2>& points);
	void segmentation(float64 based_length, float64 based_angle);

	void linkBC(std::list<const BCNode*>* bc_nodes);

	Polygons::iterator firstSegPolygon();
	Polygons::iterator lastSegPolygon();
	Polygons::iterator firstSegHole();
	Polygons::iterator lastSegHole();

	void clear();

protected:
	void make_polygons(const Curves& curves, int32 tag);
	void extract_curves(const Polygon_with_holes_2& poly, Curves& curves);
	void extract_curves(const Polygon_2& poly, Curves& curves);
	void refine_polygon_with_the_points(Polygon_2 &poly, std::list<Traits_2::Point_2>& points);
	void segment_polygon(const Polygon_2& source, Polygon_2& result, float64 based_length, float64 based_angle);
	void segment_curve(const X_monotone_curve_2& edge, std::list<X_monotone_curve_2>& result, float64 based_length, float64 based_angle, bool boundary);
	void print_polygon(const Polygon_2& poly);

	bool is_boundary_curve(const X_monotone_curve_2& edge) const;
	bool fit_based_node(const Traits_2::Point_2& pt, Traits_2::Point_2& new_pt);

private:
	// 세그멘테이션 전의 폴리곤/홀 모음
	Polygons polygons_;
	Polygons holes_;

	// 세그멘테이션 후의 폴리곤/홀 모음
	Polygons seg_polygons_;
	Polygons seg_holes_;

	// 참조하는 경계조건 노드
	std::list<const BCNode*>* ref_bcs_;
	//std::list<Traits_2::Point_2>* based_points_;
};

#include "SimpleGeometry.inl"

}
}

#endif //BZMAG_ENGINE_SIMPLEGEOMETRY_H