// 일반적으로 이 파일을 무시하지만 미리 컴파일된 헤더를 사용하는 경우 유지합니다.
#include "PolyHelper.h"
#include <CGAL/Polygon_with_holes_2.h>

using namespace bzmag;
using namespace bzmag::engine;

//-----------------------------------------------------------------------------
PolyHelper::PolyHelper() : area_(0)
{

}

//-----------------------------------------------------------------------------
PolyHelper::~PolyHelper()
{

}

//-----------------------------------------------------------------------------
void PolyHelper::clear()
{
	ID_ = -1;
	polygon_.clear();
	subdomains_.clear();
	holedomains_.clear();
	area_ = 0;
}

//-----------------------------------------------------------------------------
void PolyHelper::setPolyset(Polygon_set_2& polyset, int ID)
{
	clear();
	polygon_.setPolyset(polyset);
	ID_ = ID;
}

//-----------------------------------------------------------------------------
void PolyHelper::insertPoints(std::list<Traits_2::Point_2>& points)
{
	polygon_.insertPoints(points);
}

//-----------------------------------------------------------------------------
// 서브도메인을 구성하는동안 도메인의 전체면적이 구해진다
void PolyHelper::configSubdomains(float64 based_length, float64 based_angle, std::list<const BCNode*>* bc_nodes)
{
	area_ = 0;
	subdomains_.clear();

	// 경계조건 노드를 링크한다
	polygon_.linkBC(bc_nodes);

	// 폴리곤을 주어진 기저길이로 세그멘테이션 한다
	// 폴리곤의 arc에 대해서만 작용한다
	polygon_.segmentation(based_length, based_angle);

	// 상기 segmentation() method를 반드시 먼저 호출후 이하작업 해야한다
	SimpleGeometry::Polygons::iterator it;
	for (it = polygon_.firstSegPolygon(); it != polygon_.lastSegPolygon(); ++it)
	{
		Polygon_2& geometry = it->first;
		int32 tag = it->second;

		// 주어진 폴리곤을 홀을 제거한 폴리셋으로 바꾼다
		Polygon_set_2 polyset(geometry);
		SimpleGeometry::Polygons::iterator pp;
		for (pp = polygon_.firstSegHole(); pp != polygon_.lastSegHole(); ++pp)
		{
			if (pp->second == tag) {
				Polygon_2& poly = pp->first;
				polyset.difference(poly);
			}
		}
		
		// 폴리곤포인트 저장
		std::cout << "Find domain(" << ID() << ") point...	";
		Traits_2::Point_2 point;
		if (get_arbitrary_point_in_polygon(polyset, point)) {
			subdomains_.emplace_back(SubDomains::value_type(geometry, point));
			std::cout << point << std::endl;
		}

		// 면적계산
		float64 area = calculate_area(polyset);
		area_ = area_ + area;
	}

	// 홀 도메인 저장
	SimpleGeometry::Polygons::iterator pp;
	for (pp = polygon_.firstSegHole(); pp != polygon_.lastSegHole(); ++pp)
	{
		std::cout << "Find hole(" << ID() << ") point...		";
		Polygon_2& poly = pp->first;
		Polygon_set_2 hole(poly);
		Traits_2::Point_2 hole_pt;
		if (get_arbitrary_point_in_polygon(hole, hole_pt)) {
			holedomains_.emplace_back(SubDomains::value_type(poly, hole_pt));
			std::cout << hole_pt << std::endl;
		}
	}
}

//-----------------------------------------------------------------------------
bool PolyHelper::hitTest(const Traits_2::Point_2& pt)
{
	SubDomains::iterator it;
	for (it = subdomains_.begin(); it != subdomains_.end(); ++it)
	{
		Polygon_with_holes_2 res;
		Polygon_set_2 polyset(it->first);
		if (polyset.locate(pt, res))
			return true;
	}
	return false;
}

//-----------------------------------------------------------------------------
bool PolyHelper::get_arbitrary_point_in_polygon(const Polygon_set_2& polyset, Traits_2::Point_2& pt)
{
	// 폴리홀을 Constrained_Delaunay_triangulation 을 통해
	// 요소를 생성 한 후, 임의의 삼각형의 중심점을 리턴하기로 함
	// 이때 삼각형의 중심이 폴리곤의 내부에 있지 않을 경우, 내부에 있는 다른 점을 찾을때 까지
	// 반복하여 특정 포인트를 찾아냄
	CDT t;
	std::list<Point> based_vertices;
	extract_based_points_from_polyset(polyset, based_vertices);

	// 요소생성
	t.insert(based_vertices.begin(), based_vertices.end());

	// 요소의 중심점이 폴리곤 내부에 있으면 true와 해당 포인트(중심점) 리턴
	for (Face_circulator fiter = t.finite_faces_begin(); fiter != t.finite_faces_end(); ++fiter)
	{
		Point p1 = fiter->vertex(0)->point();
		Point p2 = fiter->vertex(1)->point();
		Point p3 = fiter->vertex(2)->point();

		// 중심점 리턴하기 위해 참조 인자에 값을 씀
		// (함수 리턴값이 false이면 아무 소용 없음)
		float64  xx = CGAL::to_double((p1.x() + p2.x() + p3.x()) / 3.0);
		float64  yy = CGAL::to_double((p1.y() + p2.y() + p3.y()) / 3.0);
		pt.set(CoordNT(xx), CoordNT(yy));

		Polygon_with_holes_2 temp;
		if (polyset.locate(pt, temp)) {
#ifdef _DEBUG
			float64 xx = CGAL::to_double(pt.x());
			float64 yy = CGAL::to_double(pt.y());
			std::cout << "Point in (" << ID_ << ") :" << pt << std::endl;
#endif
			return true;
		}
	}
	
	// 결과 리턴
	std::cout << "Fail to find arbitrary point in the polygonset which has " << t.number_of_faces() << " of faces and " << based_vertices.size() << " of vertices" << std::endl;
	return false;
}

//-----------------------------------------------------------------------------
float64 PolyHelper::calculate_area(Polygon_set_2& polyset)
{
	// 폴리곤의 면적을 구하기 위해 Poly2 정의
	typedef CGAL::Polygon_2<K>               Poly2;
	typedef CGAL::Point_2<K>                 Point2;

	float64 net_area = 0;

	std::list<Polygon_with_holes_2> res;
	polyset.polygons_with_holes(std::back_inserter(res));

	std::list<Polygon_with_holes_2>::iterator ph;
	for (ph = res.begin(); ph != res.end(); ++ph)
	{
		// 외곽경계 면적
		Polygon_with_holes_2& polyhole = *ph;

		Poly2 poly;
		const Polygon_2& outer = polyhole.outer_boundary();
		Polygon_2::Curve_const_iterator cv;
		for (cv = outer.curves_begin(); cv != outer.curves_end(); ++cv)
		{
			Traits_2::Point_2 ss = cv->source();
			poly.push_back(Point2(CGAL::to_double(ss.x()), CGAL::to_double(ss.y())));
		}
		net_area = net_area + CGAL::to_double(poly.area());

		// 내부 홀들 면적
		Polygon_with_holes_2::Hole_const_iterator hit;
		for (hit = polyhole.holes_begin(); hit != polyhole.holes_end(); ++hit)
		{
			Poly2 hole;
			const Polygon_2& inner = *hit;
			for (cv = inner.curves_begin(); cv != inner.curves_end(); ++cv)
			{
				Traits_2::Point_2 ss = cv->source();
				hole.push_back(Point2(CGAL::to_double(ss.x()), CGAL::to_double(ss.y())));
			}
			net_area = net_area + CGAL::to_double(hole.area());
		}
	}

	return net_area;
}


//-----------------------------------------------------------------------------
// Triangluation을 위함임
void PolyHelper::extract_based_points_from_polyset(const Polygon_set_2& polyset, std::list<Point>& vertices)
{
	std::list<Polygon_with_holes_2> res;
	polyset.polygons_with_holes(std::back_inserter(res));

	std::list<Polygon_with_holes_2>::iterator ph;
	for (ph = res.begin(); ph != res.end(); ++ph)
	{
		Polygon_with_holes_2& polyhole = *ph;
		extract_based_points_from_polyholes(polyhole, vertices);
	}
}

//-----------------------------------------------------------------------------
void PolyHelper::extract_based_points_from_polyholes(const Polygon_with_holes_2& polyhole, std::list<Point>& vertices)
{
	// 외곽경계 추출
	const Polygon_2& outer = polyhole.outer_boundary();

	// 경계로부터 Point 추출
	extract_based_points_from_polygon(outer, vertices);

	// 내부 홀들로 부터 Point 추출
	Polygon_with_holes_2::Hole_const_iterator hit;
	for (hit = polyhole.holes_begin(); hit != polyhole.holes_end(); ++hit)
	{
		const Polygon_2& inner = *hit;
		extract_based_points_from_polygon(inner, vertices);
	}
}

//-----------------------------------------------------------------------------
void PolyHelper::extract_based_points_from_polygon(const Polygon_2& polygon, std::list<Point>& vertices)
{
	Polygon_2::Curve_const_iterator it;
	for (it = polygon.curves_begin(); it != polygon.curves_end(); ++it)
	{
		// 기저 Edge 추출
		const X_monotone_curve_2& curve = *it;

		// 기저 Point 추출
		Traits_2::Point_2 ss = curve.source();
		Point pt(CGAL::to_double(ss.x()), CGAL::to_double(ss.y()));
		vertices.emplace_back(pt);
	}
}

