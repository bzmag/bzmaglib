//-----------------------------------------------------------------------------
inline SimpleGeometry::Polygons::iterator SimpleGeometry::firstSegPolygon()
{
	return seg_polygons_.begin();
}

//-----------------------------------------------------------------------------
inline SimpleGeometry::Polygons::iterator SimpleGeometry::lastSegPolygon()
{
	return seg_polygons_.end();
}

//-----------------------------------------------------------------------------
inline SimpleGeometry::Polygons::iterator SimpleGeometry::firstSegHole()
{
	return seg_holes_.begin();
}

//-----------------------------------------------------------------------------
inline SimpleGeometry::Polygons::iterator SimpleGeometry::lastSegHole()
{
	return seg_holes_.end();
}