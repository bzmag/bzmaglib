#ifndef BZMAG_ENGINE_COILNODE_H
#define BZMAG_ENGINE_COILNODE_H

/*
Description : Coil Node for Handling a Windings
Last Update : 2020.11.04
Author : Jaenam Bae (jaenam@dongyang.ac.kr)
*/

#include "core/node.h"
#include "core/vector2.h"

namespace bzmag
{
namespace engine
{
	class GeomHeadNode;
    class CoilNode : public Node
    {
    public:
		CoilNode();
        virtual ~CoilNode();
        DECLARE_CLASS(CoilNode, Node);

	public:
		void setDirection(bool dir);
		bool getDirection() const;
		void setNumberOfTurns(const int32 turns);
		int32 getNumberOfTurns() const;

		void setReferenceNode(GeomHeadNode* head);
		GeomHeadNode* getReferenceNode() const;
		void clear();

    public:
        virtual bool update();
        virtual void onAttachTo(Node* parent);
        virtual void onDetachFrom(Node* parent);

    public:
        static void bindMethod();
        static void bindProperty();
    


    protected:
		GeomHeadNode* ref_node_;
        bool direction_;
		int32 turns_;
    };

#include "coilnode.inl"

}
}

#endif //BZMAG_ENGINE_COILNODE_H