#include "GeomClonefromNode.h"
#include "GeomClonetoNode.h"
#include "GeomHeadNode.h"
#include "CSNode.h"
#include "core/simplepropertybinder.h"
#include "core/nodeeventpublisher.h"


using namespace bzmag;
using namespace bzmag::engine;

IMPLEMENT_CLASS(GeomCloneFromNode, GeomPrimitiveNode);

//----------------------------------------------------------------------------
GeomCloneFromNode::GeomCloneFromNode() : from_(nullptr)
{

}

//----------------------------------------------------------------------------
GeomCloneFromNode::~GeomCloneFromNode()
{

}

//----------------------------------------------------------------------------
bool GeomCloneFromNode::setReferenceNode(GeomCloneToNode* node)
{
    if(from_) from_->clones_.remove(this);

    from_ = nullptr;
    if (node) {
        node->clones_.emplace_back(this);
        node->clones_.unique();
        from_ = node;
	}

	return update();
}

//----------------------------------------------------------------------------
GeomCloneToNode* GeomCloneFromNode::getReferenceNode() const
{
    return from_;
}

//----------------------------------------------------------------------------
bool GeomCloneFromNode::make_geometry(Polygon_set_2& polyset, Curves& curves, Vertices& vertices, Transformation transform)
{
	if (from_) {
		from_->makeGeometry(transform);
		polyset = from_->getPolyset();
		curves = from_->getCurves();
		vertices = from_->getVertices();
		return true;
	}

	// From이 없으면 Geometry를 생성할 수 없음
	return false;
}

//----------------------------------------------------------------------------
void GeomCloneFromNode::updateCovered()
{
	if(from_) bCovered_ = from_->isCovered();
	else bCovered_ = false;
}

//----------------------------------------------------------------------------
Transformation GeomCloneFromNode::getMyTransform()
{
	return Transformation();
}

//----------------------------------------------------------------------------
void GeomCloneFromNode::bindProperty()
{

}