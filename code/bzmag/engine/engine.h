#ifndef BZMAG_ENGINE_H
#define BZMAG_ENGINE_H

#include "GeomHeadNode.h"

#include "GeomPrimitiveNode.h"
#include "GeomCurveNode.h"
#include "GeomCircleNode.h"
#include "GeomRectNode.h"
#include "GeomBandNode.h"
#include "GeomClonefromNode.h"

#include "GeomClonetoNode.h"
#include "GeomCoverlineNode.h"
#include "GeomMoveNode.h"
#include "GeomRotateNode.h"
#include "GeomSpiltNode.h"

#include "GeomSubtractNode.h"
#include "GeomUniteNode.h"
#include "GeomIntersectionNode.h"

#include "CSnode.h"
#include "BCnode.h"
#include "MaterialNode.h"
#include "CoilNode.h"
#include "WindingNode.h"
#include "expressionServer.h"

#include "GeometryToTriangle.h"

#include "core/define.h"
#include "core/module.h"
#include "core/kernel.h"

#endif //BZMAG_ENGINE_H