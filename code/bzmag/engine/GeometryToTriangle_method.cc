#include "GeometryToTriangle.h"
#include "core/methodbinder.h"

using namespace bzmag;
using namespace bzmag::engine;

//----------------------------------------------------------------------------
static void GeometryToTriangle_b_makePolyStructure_s(GeometryToTriangle* self, Parameter* param)
{
	bool result = self->makePolyStructures(param->in()->get<String>(0));
	result = self->makePolyStructures(param->in()->get<String>(0));
	param->out()->get<bool>(0) = result;
}

//----------------------------------------------------------------------------
static void GeometryToTriangle_v_linkBC_s(GeometryToTriangle* self, Parameter* param)
{
	self->linkBoundaryConditions(param->in()->get<String>(0));
}

//----------------------------------------------------------------------------
static void GeometryToTriangle_d_getDomainArea_i(GeometryToTriangle* self, Parameter* param)
{
	uint32 ID = param->in()->get<uint32>(0);
	param->out()->get<float64>(0) = self->area(ID);
}

//----------------------------------------------------------------------------
static void GeometryToTriangle_z_getVertices_v(GeometryToTriangle* self, Parameter* param)
{
    param->out()->clear();

	GeometryToTriangle::Vertices::iterator vv;
	for (vv = self->firstVertex(); vv != self->lastVertex(); ++vv)
	{
		GeometryToTriangle::Vert v = *vv;
		param->out()->add<float64>(v.x_);
		param->out()->add<float64>(v.y_);
	}
}

//----------------------------------------------------------------------------
static void GeometryToTriangle_z_getSegments_v(GeometryToTriangle* self, Parameter* param)
{
	param->out()->clear();

	GeometryToTriangle::Segments::iterator ss;
	for (ss = self->firstSegment(); ss != self->lastSegment(); ++ss)
	{
		GeometryToTriangle::Seg s = *ss;
		param->out()->add<int32>(s.first);
		param->out()->add<int32>(s.second);
	}
}

//----------------------------------------------------------------------------
static void GeometryToTriangle_z_getSegmentMarkers_v(GeometryToTriangle* self, Parameter* param)
{
	param->out()->clear();

	std::vector<int64>::iterator ss;
	for (ss = self->firstSegmentMarker(); ss != self->lastSegmentMarker(); ++ss)
	{
		int32 s = (int32)*ss;
		param->out()->add<int32>(s);
	}
}

//----------------------------------------------------------------------------
static void GeometryToTriangle_z_getRegions_v(GeometryToTriangle* self, Parameter* param)
{
	param->out()->clear();

	GeometryToTriangle::Regions::iterator vv;
	for (vv = self->firstRegion(); vv != self->lastRegion(); ++vv)
	{
		GeometryToTriangle::Reg reg = *vv;
		int32 ID = reg.first;
		float64 area = self->getMaximumAreaOfElements(ID);
		GeometryToTriangle::Vert& v = reg.second;
		param->out()->add<float64>(v.x_);
		param->out()->add<float64>(v.y_);
		param->out()->add<int32>(ID);
		param->out()->add<float64>(area);
	}
}

//----------------------------------------------------------------------------
static void GeometryToTriangle_z_getHoles_v(GeometryToTriangle* self, Parameter* param)
{
	param->out()->clear();

	GeometryToTriangle::Points::iterator pp;
	for (pp = self->firstHole(); pp != self->lastHole(); ++pp)
	{
		Traits_2::Point_2 p = *pp;
		param->out()->add<float64>(CGAL::to_double(p.x()));
		param->out()->add<float64>(CGAL::to_double(p.y()));
	}
}

//----------------------------------------------------------------------------
void GeometryToTriangle::bindMethod()
{
	BIND_METHOD(v_linkBC_s, GeometryToTriangle_v_linkBC_s);
	BIND_METHOD(b_makePolyStructure_s, GeometryToTriangle_b_makePolyStructure_s);
	BIND_METHOD(d_getDomainArea_i, GeometryToTriangle_d_getDomainArea_i);
    BIND_METHOD(z_getVertices_v, GeometryToTriangle_z_getVertices_v);
	BIND_METHOD(z_getSegments_v, GeometryToTriangle_z_getSegments_v);
	BIND_METHOD(z_getSegmentMarkers_v, GeometryToTriangle_z_getSegmentMarkers_v);
	BIND_METHOD(z_getRegions_v, GeometryToTriangle_z_getRegions_v);
	BIND_METHOD(z_getHoles_v, GeometryToTriangle_z_getHoles_v);
}
