//-----------------------------------------------------------------------------
inline GeometryToTriangle::Vertices::iterator GeometryToTriangle::firstVertex()
{
	return vertices_.begin();
}

//-----------------------------------------------------------------------------
inline GeometryToTriangle::Vertices::iterator GeometryToTriangle::lastVertex()
{
	return vertices_.end();
}

//-----------------------------------------------------------------------------
inline GeometryToTriangle::Segments::iterator GeometryToTriangle::firstSegment()
{
	return segments_.begin();
}

//-----------------------------------------------------------------------------
inline GeometryToTriangle::Segments::iterator GeometryToTriangle::lastSegment()
{
	return segments_.end();
}

//-----------------------------------------------------------------------------
inline std::vector<int64>::iterator GeometryToTriangle::firstSegmentMarker()
{
	return segment_makers_.begin();
}

//-----------------------------------------------------------------------------
inline std::vector<int64>::iterator GeometryToTriangle::lastSegmentMarker()
{
	return segment_makers_.end();
}

//-----------------------------------------------------------------------------
inline GeometryToTriangle::Regions::iterator GeometryToTriangle::firstRegion()
{
	return regions_.begin();
}

//-----------------------------------------------------------------------------
inline GeometryToTriangle::Regions::iterator GeometryToTriangle::lastRegion()
{
	return regions_.end();
}

//-----------------------------------------------------------------------------
inline GeometryToTriangle::Points::iterator GeometryToTriangle::firstHole()
{
	return holes_.begin();
}

//-----------------------------------------------------------------------------
inline GeometryToTriangle::Points::iterator GeometryToTriangle::lastHole()
{
	return holes_.end();
}
