#include "GeomHeadNode.h"
#include "GeomBooleanNode.h"
#include "CSNode.h"
#include "MaterialNode.h"

#include "core/simplepropertybinder.h"
#include "core/nodeeventpublisher.h"

using namespace bzmag;
using namespace bzmag::engine;

IMPLEMENT_CLASS(GeomHeadNode, GeomBaseNode);

//----------------------------------------------------------------------------
GeomHeadNode::GeomHeadNode() :
    bStandalone_(true),
    bModelNode_(true),
	bHide_(false),
    color_(230, 230, 230, 200),
    num_elements_(0),
	lastNode_(nullptr)
{
    
}

//----------------------------------------------------------------------------
GeomHeadNode::~GeomHeadNode()
{

}

//----------------------------------------------------------------------------
bool GeomHeadNode::contain(GeomHeadNode* node)
{
	const Polygon_set_2& geometry = getPolyset();
	const Polygon_set_2& geom = node->getPolyset();

	Polygon_set_2 op(geom);
	op.difference(geometry);
	if (op.is_empty())
		return true;

	return false;
}

//----------------------------------------------------------------------------
void GeomHeadNode::setLastNode(GeomBaseNode* last)
{
	lastNode_ = last;
	bCovered_ = last->bCovered_;
	bGeometry_ = false;

	GeomBaseNode* parent = dynamic_cast<GeomBaseNode*>(getParent());
	if (parent) parent->update();
}

//----------------------------------------------------------------------------
GeomBaseNode* GeomHeadNode::getLastNode() const
{
	return lastNode_;
}

//----------------------------------------------------------------------------
void GeomHeadNode::setMaterialNode(Node* material)
{
    material_ = material;
}

//----------------------------------------------------------------------------
Node* GeomHeadNode::getMaterialNode() const
{
    return material_;
}

//----------------------------------------------------------------------------
bool GeomHeadNode::makeGeometry(Transformation trans/* = Transformation()*/)
{
	GeomBaseNode* last = getLastNode();

	bool result = last->makeGeometry(trans);
	geometry_ = last->geometry_;
	curves_   = last->curves_;
	vertices_ = last->vertices_;

	bGeometry_ = true;
	return result;
}

//----------------------------------------------------------------------------
bool GeomHeadNode::make_geometry(Polygon_set_2& polyset, Curves& curves, Vertices& vertices, Transformation transform)
{
	return true;
}

//----------------------------------------------------------------------------
Transformation GeomHeadNode::getMyTransform()
{
	return Transformation();
}

//----------------------------------------------------------------------------
void GeomHeadNode::updateTransform()
{
	last_trans_ = Transformation();
}

//----------------------------------------------------------------------------
void GeomHeadNode::updateLinkedNode()
{
	linked_heads_.clear();
}

//----------------------------------------------------------------------------
bool GeomHeadNode::update()
{
	// 업데이트가 발생하면 makeGeometry()를 새로 호출해야 한다
	bGeometry_ = false;

	// 해드, 변환 메트릭스, 링크 노드 초기화 : 부모로부터 가져오기
	GeomBaseNode* parent = dynamic_cast<GeomBaseNode*>(getParent());
	if (parent) {
		// 1. 해드노드 초기화
		head_ = parent->head_;

		// 2. 최종변환 메트릭스 초기화
		last_trans_ = parent->last_trans_;

		// 3. 링크노드 초기화
		linked_heads_ = parent->linked_heads_;

		// 4. 커버드 초기화
		bCovered_ = parent->bCovered_;
	}
	else {
		head_ = nullptr;
		last_trans_ = Transformation();
		linked_heads_.clear();
		bCovered_ = false;
	}

	// 1. 해드노드 업데이트 
	updateHead();

	// 2. 최종변환 메트릭스 업데이트
	updateTransform();

	// 3. 링크노드 업데이트
	updateLinkedNode();

	// 4. 커버드 업데이트
	updateCovered();

	return true;
}

//----------------------------------------------------------------------------
void GeomHeadNode::onAttachTo(Node* parent)
{
	// 부모가 Boolean 노드라면 부모 업데이트
	GeomBooleanNode* node = dynamic_cast<GeomBooleanNode*>(parent);
	if (node) {
		node->update();
	}
}

//----------------------------------------------------------------------------
void GeomHeadNode::onDetachFrom(Node* parent)
{
	// 부모가 Boolean 노드라면 부모 업데이트
	GeomBooleanNode* node = dynamic_cast<GeomBooleanNode*>(parent);
	if (node) {
		node->update();
	}
}

//----------------------------------------------------------------------------
void GeomHeadNode::bindProperty()
{
    BIND_PROPERTY(const Color&, Color, &setColor, &getColor);
    BIND_PROPERTY(bool, IsHide, &setHideStatus, &isHide);
    BIND_PROPERTY(bool, IsModel, &setModelNode, &isModelNode);
    BIND_PROPERTY(bool, IsStandAlone, 0, &isStandAlone);
    BIND_PROPERTY(Node*, Material, &setMaterialNode, &getMaterialNode);
    BIND_PROPERTY(int32, RequiredNumberOfElements, &setNumberOfElements, &getNumberOfElements);
}

