#include "GeometryToTriangle.h"
#include "GeomHeadNode.h"
#include "core/simplepropertybinder.h"

using namespace bzmag;
using namespace bzmag::engine;


IMPLEMENT_CLASS(GeometryToTriangle, Node);

int32 GeometryToTriangle::based_elements_num_ = 6000;
float64 GeometryToTriangle::based_angle_ = 3;		// deg단위
float64 GeometryToTriangle::tol_ = 1e-9;

//-----------------------------------------------------------------------------
GeometryToTriangle::GeometryToTriangle()
{

}

//-----------------------------------------------------------------------------
GeometryToTriangle::~GeometryToTriangle()
{

}

//-----------------------------------------------------------------------------
void GeometryToTriangle::clear()
{
	polysets_.clear();
	zorder_polysets_.clear();
	poly_units_.clear();
	points_.clear();
	required_elements_.clear();

	vertices_.clear();
	vertices_maker_.clear();
	segments_.clear();
	segment_makers_.clear();
	regions_.clear();
	holes_.clear();

	whole_domain_.clear();
}

//-----------------------------------------------------------------------------
void GeometryToTriangle::setBasedNumberOfElements(int32 num_e)
{
	based_elements_num_ = num_e;
}

//-----------------------------------------------------------------------------
int32 GeometryToTriangle::getBasedNumberOfElements() const
{
	return based_elements_num_;
}

//-----------------------------------------------------------------------------
void GeometryToTriangle::setBasedAngle(float64 angle)
{
	based_angle_ = angle;
}

//-----------------------------------------------------------------------------
float64 GeometryToTriangle::getBasedAngle() const
{
	return based_angle_;
}

//-----------------------------------------------------------------------------
void GeometryToTriangle::setTolerance(float64 tol)
{
	tol_ = tol;
}

//-----------------------------------------------------------------------------
float64 GeometryToTriangle::getTolerance() const
{
	return tol_;
}

//-----------------------------------------------------------------------------
PolyHelper* GeometryToTriangle::findPolyHelper(int32 ID)
{
	PolyUnits::iterator& f =
		std::find_if(poly_units_.begin(), poly_units_.end(), std::bind2nd(FindPolyHelper(), ID));

	if (f == poly_units_.end()) return nullptr;
	PolyHelper* helper = &(*f);
	return helper;
}

//-----------------------------------------------------------------------------
float64 GeometryToTriangle::area(int32 ID)
{
	PolyHelper* helper = findPolyHelper(ID);
	if (helper) {
		return helper->area();
	}
	else {
		return 0;
	}
}

//-----------------------------------------------------------------------------
float64 GeometryToTriangle::getMaximumAreaOfElements(int32 ID)
{
	float64 req_area = 0;
	if (required_elements_[ID] > 0) {
		req_area = area(ID) / float64(required_elements_[ID]);
	}
	return req_area;
}

//-----------------------------------------------------------------------------
void GeometryToTriangle::linkBoundaryConditions(const String& path)
{
	Kernel* kernel = Kernel::instance();
	Node* node = kernel->lookup(path);

	bc_nodes_.clear();
	set_root_BC_node(node);
}

//-----------------------------------------------------------------------------
bool GeometryToTriangle::makePolyStructures(const String& path)
{
	Kernel* kernel = Kernel::instance();
	Node* node = kernel->lookup(path);

	if (0 == node) return false;
	clear();
	set_root_node(node);

	if (!valid()) return false;

	// 이제까지 추가된 Polyset들을 기반으로 Triangle 입력을 만든다
	// 우선 기저절점을 모두 뽑아내고, 
	// 각 Polyset의 z-order를 계산한다.
	PolySets::iterator ps;
	for (ps = polysets_.begin(); ps != polysets_.end(); ++ps)
	{
		Polyset& polyset = *ps;
		extract_points(polyset.second);
		if (!config_zorder(polyset))
			return false;
	}

	// 요소의 기준 길이를 얻는다 (모델 최외곽 박스 기준 5000개 요소)
	float64 based_length = calculate_based_segment_length();

	// z-order가 계산되었으면
	// 순서에 맞게 빼기작업을 하여 서로 겹치는 오브젝트가 없도록 만든다
	make_zorder_polyset(based_length, based_angle_);

	// Triangle 입력 구조를 만든다; Vertex, Region 등등...
	PolyUnits::iterator pu;
	for (pu = poly_units_.begin(); pu != poly_units_.end(); ++pu)
	{
		PolyHelper& helper = *pu;
		std::cout << "ID:" << helper.ID() << ", Area: " << helper.area() << std::endl;

		PolyHelper::SubDomains::iterator sd;
		for (sd = helper.firstSubDomain(); sd != helper.lastSubDomain(); ++sd)
		{
			PolyHelper::SubDomain& sub = *sd;
			Polygon_2& polygon = sub.first;

			Traits_2::Point_2 pt = sub.second;
			make_triangle_input(polygon, helper.ID(), pt);
		}
	}

	find_holes(based_length);
	//approximate_vertices();
	erase_duplicated_points();
	erase_duplicated_segments();
	
	return true;
}

//-----------------------------------------------------------------------------
void GeometryToTriangle::set_root_BC_node(const Node* node)
{
	if (0 == node) return;

	Node::ConstNodeIterator it;
	for (it = node->firstChildNode(); it != node->lastChildNode(); ++it)
	{
		Node* child = *it;
		const BCNode* BC = dynamic_cast<BCNode*>(child);
		if (BC) {
			bc_nodes_.push_back(BC);
		}
	}
}

//-----------------------------------------------------------------------------
void GeometryToTriangle::set_root_node(const Node* node)
{
	if (0 == node) return;

	Node::ConstNodeIterator it;
	for (it = node->firstChildNode(); it != node->lastChildNode(); ++it)
	{
		Node* childnode = *it;
		GeomHeadNode* head = dynamic_cast<GeomHeadNode*>(childnode);
		if (head && head->isModelNode() && head->isStandAlone())
		{
			int32 ID = head->getID();
			const Polygon_set_2& polyset = head->getPolyset();
			polysets_.push_back(Polyset(ID, polyset));

			// 요구되는 요소갯수 저장 ; [ID:갯수]
			required_elements_[ID] = head->getNumberOfElements();

			// Whole Domain에 추가 --> 추후 Hole 판별에 사용할 것임
			whole_domain_.join(polyset);
		}
	}
}

//-----------------------------------------------------------------------------
void GeometryToTriangle::make_triangle_input(Polygon_2& polygon, int32 ID, Traits_2::Point_2 pt)
{
	Polygon_2::Curve_const_iterator cv = polygon.curves_begin();
	Traits_2::Point_2 s = cv->source();
	Vert vert_first(CGAL::to_double(s.x()), CGAL::to_double(s.y()));

	vertices_.push_back(vert_first);
	int32 vID_start = int32(vertices_.size() - 1);

	for (++cv; cv != polygon.curves_end(); ++cv)
	{
		Traits_2::Point_2 ss = cv->source();
		Vert vert(CGAL::to_double(ss.x()), CGAL::to_double(ss.y()));
		vertices_.push_back(vert);

		// Zero start index
		Seg seg(int32(vertices_.size() - 2), int32(vertices_.size() - 1));
		segments_.push_back(seg);

		// 세그먼트에 대한 바운드리마커 설정
		Vert vss = vertices_[seg.first];
		Vert vtt = vertices_[seg.second];
		const BCNode* BC = test_segment_on_BC(vss, vtt);
		if (BC) {
			segment_makers_.push_back(BC->getID());
		}
		else {
			segment_makers_.push_back(0);
		}
		
	}
	Seg seg(int32(vertices_.size() - 1), vID_start);
	segments_.push_back(seg);

	// 세그먼트에 대한 바운드리마커 설정
	Vert vss = vertices_[seg.first];
	Vert vtt = vertices_[seg.second];
	const BCNode* BC = test_segment_on_BC(vss, vtt);
	if (BC) {
		segment_makers_.push_back(BC->getID());
	}
	else {
		segment_makers_.push_back(0);
	}

	// 영역 만들기
	Vert reg_pt(CGAL::to_double(pt.x()), CGAL::to_double(pt.y()));
	regions_.push_back(Reg(ID, reg_pt));
}

//-----------------------------------------------------------------------------
float64 GeometryToTriangle::calculate_based_segment_length(int32 num_elements)
{
	float64 min_x = 0, min_y = 0, max_x = 0, max_y = 0;
	PolySets::iterator ps;
	for (ps = polysets_.begin(); ps != polysets_.end(); ++ps)
	{
		Polygon_set_2& polyset = (*ps).second;

		std::list<Polygon_with_holes_2> res;
		polyset.polygons_with_holes(std::back_inserter(res));

		std::list<Polygon_with_holes_2>::iterator it;
		for (it = res.begin(); it != res.end(); ++it)
		{
			Rectangle_2 bbox = (*it).outer_boundary().bbox();
			Point_2 local_min = bbox.min();
			Point_2 local_max = bbox.max();
			float64 minx = CGAL::to_double(local_min.x());
			float64 miny = CGAL::to_double(local_min.y());
			float64 maxx = CGAL::to_double(local_max.x());
			float64 maxy = CGAL::to_double(local_max.y());

			if (minx < min_x) min_x = minx;
			if (miny < min_y) min_y = miny;
			if (maxx > max_x) max_x = maxx;
			if (maxy > max_y) max_y = maxy;
		}
	}
	float64 area = abs((max_x - min_x) * (max_y - min_y));
	return std::sqrt(area / (float64)num_elements);
}

//-----------------------------------------------------------------------------
// 각 Polyset들이 완전종속 혹은 완전독립임을 확인한다.
bool GeometryToTriangle::valid()
{
	PolySets::iterator ps;
	for (ps = polysets_.begin(); ps != polysets_.end(); ++ps)
	{
		Polyset& polyset = *ps;
		if (!valid(polyset)) return false;
	}
	return true;
}

//-----------------------------------------------------------------------------
bool GeometryToTriangle::valid(Polyset& polyset)
{
	PolySets::iterator it;
	for (it = polysets_.begin(); it != polysets_.end(); ++it)
	{
		Polyset& ps = *it;
		if (polyset.first == ps.first) continue;

		// A-B = 0 || B-A = 0 이면 포함관계 이므로 참
		Polygon_set_2 A(polyset.second);
		Polygon_set_2 B(ps.second);
		A.difference(B);
		B.difference(polyset.second);
		if (A.is_empty() || B.is_empty()) continue;

		// 공통 부분이 없다면 서로 완전 독립이므로 참
		Polygon_set_2 AA(polyset.second);
		Polygon_set_2 BB(ps.second);
		AA.intersection(BB);
		if (AA.is_empty()) continue;
		else {
			std::cout << "Object " << polyset.first << " and " << ps.first << " are overlaped!" << std::endl;
			//std::cout << AA.number_of_polygons_with_holes() << "," << BB.number_of_polygons_with_holes() << std::endl;
			return false;
		}
	}

	return true;
}

//-----------------------------------------------------------------------------
bool GeometryToTriangle::config_zorder(Polyset& polyset)
{
	if (zorder_polysets_.size() == 0) {
		zorder_polysets_.push_back(polyset);
	}
	else {
		PolySets::iterator ps;
		for (ps = zorder_polysets_.begin(); ps != zorder_polysets_.end(); ++ps)
		{
			Polygon_set_2 A = ps->second;
			A.difference(polyset.second);
			if (!A.is_empty()) {
				Polygon_set_2 AA = ps->second;
				AA.difference(A);
				if (!AA.is_empty())
					break;
			}
		}

		zorder_polysets_.emplace(ps, polyset);
	}

	return true;
}

//-----------------------------------------------------------------------------
void GeometryToTriangle::make_zorder_polyset(float64 based_length, float64 based_angle)
{
	while (zorder_polysets_.size() > 0) {
		Polyset polyset = zorder_polysets_.back();
		zorder_polysets_.pop_back();

		PolySets::reverse_iterator it;
		for (it = zorder_polysets_.rbegin(); it != zorder_polysets_.rend(); ++it) {
			polyset.second.difference(it->second);
		}

		PolyHelper geometry_helper;
		//std::cout << "Polyset ID :" << polyset.first << std::endl;

		// 1. 폴리셋트와 ID를 설정하고
		geometry_helper.setPolyset(polyset.second, polyset.first);

		// 2. 기저절점을 삽입하여 세그먼트 분할을 하고
		geometry_helper.insertPoints(points_);

		// 3. 최종적으로 서브도메인(도메인상의 임의점 포함) 구성
		//    Segmentation / calculate area / get arbitrary point inside
		//    경계조건을 고려해 세그멘테이션 하며, 경계 절점/엣지에는 바운드리 마커 설정
		geometry_helper.configSubdomains(based_length, based_angle, &bc_nodes_);

		// 구성된 GeometryHelper 삽입
		poly_units_.push_back(geometry_helper);
	}
}

//-----------------------------------------------------------------------------
void GeometryToTriangle::extract_points(Polygon_set_2& polyset)
{
	std::list<Polygon_with_holes_2> res;
	polyset.polygons_with_holes(std::back_inserter(res));

	std::list<Polygon_with_holes_2>::iterator it;
	for (it = res.begin(); it != res.end(); ++it)
	{
		Polygon_with_holes_2& polyhole = *it;
		extract_based_points_from_polyholes(polyhole);
	}

	points_.unique();
}

//-----------------------------------------------------------------------------
void GeometryToTriangle::extract_based_points_from_polyholes(Polygon_with_holes_2& polyhole)
{
	// 외곽경계 추출
	const Polygon_2& outer = polyhole.outer_boundary();

	// 경계로부터 Point 추출
	extract_based_points_from_polygon(outer);

	// 내부 홀들로 부터 Point 추출
	Polygon_with_holes_2::Hole_const_iterator hit;
	for (hit = polyhole.holes_begin(); hit != polyhole.holes_end(); ++hit)
	{
		const Polygon_2& inner = *hit;
		extract_based_points_from_polygon(inner);
	}
}

//-----------------------------------------------------------------------------
void GeometryToTriangle::extract_based_points_from_polygon(const Polygon_2& polygon)
{
	Polygon_2::Curve_const_iterator it;
	for (it = polygon.curves_begin(); it != polygon.curves_end(); ++it)
	{
		// 기저 Edge 추출
		const X_monotone_curve_2 curve = *it;

		// 기저 Point 추출
		Traits_2::Point_2 ss = curve.source();
		Traits_2::Point_2 tt = curve.target();
		points_.push_back(ss);
		points_.push_back(tt);
	}
}

//-----------------------------------------------------------------------------
void GeometryToTriangle::approximate_vertices()
{
	Vertices::iterator vv;
	for (vv = vertices_.begin(); vv != vertices_.end(); ++vv)
	{
		Vert& v = *vv;
		v.x_ = String("%.5e", v.x_).toDouble();
		v.y_ = String("%.5e", v.y_).toDouble();
	}
}


//-----------------------------------------------------------------------------
void GeometryToTriangle::erase_duplicated_points()
{
	Vertices new_vertices;
	std::map<int32, int32> IDMaps;
	
	int32 ID = 0;
	int32 newID = 0;
	Vertices::iterator vv;
	for (vv = vertices_.begin(); vv != vertices_.end(); ++vv, ++ID)
	{
		Vert v = *vv;
		Vertices::const_iterator fv = find_vertex(v, new_vertices);
		if (fv == new_vertices.end()) {
			IDMaps[ID] = newID;
			newID = newID + 1;
			new_vertices.push_back(v);
		}
		else {
			int32 findID = (int32)std::distance(new_vertices.cbegin(), fv);
			IDMaps[ID] = findID;
		}
	}
	vertices_ = new_vertices;

	Segments::iterator ss;
	for (ss = segments_.begin(); ss != segments_.end(); ++ss)
	{
		Seg& s = *ss;
		s.first  = IDMaps[s.first];
		s.second = IDMaps[s.second];
	}
}

//-----------------------------------------------------------------------------
GeometryToTriangle::Vertices::const_iterator GeometryToTriangle::find_vertex(const Vert& v, const Vertices& vertices)
{
	int32 ID = 0;
	Vertices::const_iterator f;
	for (f = vertices.begin(); f != vertices.end(); ++f, ++ID)
	{
		Vert v1 = *f;
		float64 dx = v1.x_ - v.x_;
		float64 dy = v1.y_ - v.y_;
		float64 dl = std::sqrt(dx * dx + dy * dy);
		if (dl < tol_) {
			break;
		}
	}
	return f;
}

//-----------------------------------------------------------------------------
void GeometryToTriangle::erase_duplicated_segments()
{
	int32 ID = 0;
	Segments::iterator ss;
	for (ss = segments_.begin(); ss != segments_.end(); ++ID)
	{
		Seg s = *ss;
		Segments::const_iterator fs = find_segment(s, segments_);
		if (fs != ss) {
			ss = segments_.erase(ss);
			int32 idx = (int32)std::distance(segments_.begin(), ss);
			segment_makers_.erase(segment_makers_.begin() + idx);
		}
		else {
			++ss;
		}
	}
}

//-----------------------------------------------------------------------------
GeometryToTriangle::Segments::const_iterator GeometryToTriangle::find_segment(const Seg& s, const Segments& segments)
{
	Segments::const_iterator f;
	for (f = segments.begin(); f != segments.end(); ++f)
	{
		Seg s1 = *f;
		Seg s2(s1.second, s1.first);
		if ((s == s1) || (s == s2))
			break;
	}
	return f;
}

//-----------------------------------------------------------------------------
void GeometryToTriangle::find_holes(float64 based_length, float64 based_angle)
{
	std::cout << "Find Holes..." << std::endl;

	PolyHelper helper;

	// Hole을 찾을때는 ID를 -1로 둔다; 사실 아무값이나 사용해도 무방함
	helper.setPolyset(whole_domain_, -1);

	// subDomain 구성하는 동안 Region과 Hole이 구성되며
	// 동시에 세그멘테이션(바운드리마커설정포함) 된다
	// 여기서는 경계조건에 대한 고려가 필요 없기때문에 nullptr로 설정한다
	helper.configSubdomains(based_length, based_angle, nullptr);

	// 생성된 subDomain 중 Hole에 대해서만 작업한다
	PolyHelper::SubDomains::iterator it;
	for (it = helper.firstHoleDomain(); it != helper.lastHoleDomain(); ++it)
	{
		PolyHelper::SubDomain& holedomain = *it;
		Polygon_2& hole = holedomain.first;
		Traits_2::Point_2& pt = holedomain.second;

		holes_.push_back(pt);
	}
}

//----------------------------------------------------------------------------
const BCNode* GeometryToTriangle::test_segment_on_BC(const Vert& ss, const Vert& tt)
{
	BCNodes::iterator it;
	for (it = bc_nodes_.begin(); it != bc_nodes_.end(); ++it)
	{
		const BCNode* BC = *it;

		Traits_2::Point_2 s(CoordNT(ss.x_), CoordNT(ss.y_));
		Traits_2::Point_2 t(CoordNT(tt.x_), CoordNT(tt.y_));

		if (BC->testSegment(s, t)) {
			return BC;
		}
	}

	return nullptr;
}

//----------------------------------------------------------------------------
void GeometryToTriangle::bindProperty()
{
	BIND_PROPERTY(int32, BasedNumberOfElements, &setBasedNumberOfElements, &getBasedNumberOfElements);
	BIND_PROPERTY(float64, BasedAngle, &setBasedAngle, &getBasedAngle);
	BIND_PROPERTY(float64, Tolerance, &setTolerance, &getTolerance);
}
