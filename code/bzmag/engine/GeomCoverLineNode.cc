#include "GeomCoverlineNode.h"

#include "core/simplepropertybinder.h"
#include "core/nodeeventpublisher.h"

using namespace bzmag;
using namespace bzmag::engine;


IMPLEMENT_CLASS(GeomCoverLineNode, GeomBaseNode);

//----------------------------------------------------------------------------
GeomCoverLineNode::GeomCoverLineNode()
{
	bCovered_ =  true;
}

//----------------------------------------------------------------------------
GeomCoverLineNode::~GeomCoverLineNode()
{

}

//----------------------------------------------------------------------------
bool GeomCoverLineNode::make_geometry(Polygon_set_2& polyset, Curves& curves, Vertices& vertices, Transformation transform)
{
	return true;
}

//----------------------------------------------------------------------------
Transformation GeomCoverLineNode::getMyTransform()
{
	return Transformation();
}

//----------------------------------------------------------------------------
void GeomCoverLineNode::updateCovered()
{
	bCovered_ = true;
}

//----------------------------------------------------------------------------
void GeomCoverLineNode::bindProperty()
{

}
