#include "SimpleGeometry.h"
#include "print_utils.h"

using namespace bzmag;
using namespace bzmag::engine;

//-----------------------------------------------------------------------------
SimpleGeometry::SimpleGeometry() : ref_bcs_(nullptr)//, based_points_(nullptr)
{

}

//-----------------------------------------------------------------------------
SimpleGeometry::~SimpleGeometry()
{

}

//-----------------------------------------------------------------------------
void SimpleGeometry::clear()
{
	polygons_.clear();
	holes_.clear();

	seg_polygons_.clear();
	seg_holes_.clear();

	ref_bcs_ = nullptr;
	//based_points_ = nullptr;
}

//-----------------------------------------------------------------------------
void SimpleGeometry::setPolyset(Polygon_set_2& geometry)
{
	clear();

	// polygons_with_holes --> 리스트 형태의 polygon_with_holes로 변환
	std::list<Polygon_with_holes_2> res;
	geometry.polygons_with_holes(std::back_inserter(res));

	std::list<Polygon_with_holes_2>::const_iterator it;
	for (it = res.begin(); it != res.end(); ++it)
	{
		const Polygon_with_holes_2& polyhole = *it;

		Curves curves;
		extract_curves(polyhole, curves);

		int32 tag = (int32)std::distance(res.cbegin(), it);
		make_polygons(curves, tag);
	}

	
}

//-----------------------------------------------------------------------------
void SimpleGeometry::extract_curves(const Polygon_with_holes_2& polyhole, Curves& curves)
{
	// 외곽경계 추출
	const Polygon_2& outer = polyhole.outer_boundary();

	// 경계로부터 Point 추출
	extract_curves(outer, curves);

	// 내부 홀들로 부터 Point 추출
	Polygon_with_holes_2::Hole_const_iterator hit;
	for (hit = polyhole.holes_begin(); hit != polyhole.holes_end(); ++hit)
	{
		const Polygon_2& inner = *hit;
		extract_curves(inner, curves);
	}
}

//-----------------------------------------------------------------------------
void SimpleGeometry::extract_curves(const Polygon_2& poly, Curves& curves)
{
	Polygon_2::Curve_const_iterator it;
	for (it = poly.curves_begin(); it != poly.curves_end(); ++it)
	{
		const X_monotone_curve_2& curve = *it;
		curves.push_back(curve);
	}
}

//-----------------------------------------------------------------------------
void SimpleGeometry::make_polygons(const Curves& curves, int32 tag)
{
	Arrangement arr;
	CGAL::insert(arr, curves.begin(), curves.end());
	Arrangement::Face_const_iterator ff;
	for (ff = arr.faces_begin(); ff != arr.faces_end(); ++ff) {
		Arrangement::Face face = *ff;

		if (face.has_outer_ccb())
		{
			std::list<X_monotone_curve_2> poly;
			Arrangement::Ccb_halfedge_const_circulator curr = ff->outer_ccb();
			X_monotone_curve_2 curve = curr->curve();
			poly.push_back(curve);
			Traits_2::Point_2 ss = curve.source();
			Traits_2::Point_2 tt = curve.target();
			curr++;

			while (curr != ff->outer_ccb()) {
				curve = curr->curve();

				// 이전 끝점이 지금 커브의 시작점이면
				if (tt == curve.source())
					poly.push_back(curve);

				// 지금커브의 끝점이 이전커브의 시작점이면
				else
					poly.push_front(curve);

				ss = curve.source();
				tt = curve.target();
				++curr;
			}

			Polygon_2 polygon(poly.begin(), poly.end());
			//print_polygon(polygon);
			if (polygon.orientation() == CGAL::COUNTERCLOCKWISE) {
				polygons_.push_back(Polygon_tag(polygon, tag));
			}
			else {
				polygon.reverse_orientation();
				holes_.push_back(Polygon_tag(polygon, tag));
			}
		}
	}
}

//-----------------------------------------------------------------------------
bool SimpleGeometry::insertPoints(std::list<Traits_2::Point_2>& points)
{
	//based_points_ = &points;

	Polygons::iterator it;
	for (it = polygons_.begin(); it != polygons_.end(); ++it)
	{
		Polygon_2 &poly = it->first;
		refine_polygon_with_the_points(poly, points);
	}

	for (it = holes_.begin(); it != holes_.end(); ++it)
	{
		Polygon_2 &poly = it->first;
		refine_polygon_with_the_points(poly, points);
	}

	return true;
}

//-----------------------------------------------------------------------------
void SimpleGeometry::refine_polygon_with_the_points(Polygon_2 &poly,
	std::list<Traits_2::Point_2>& points)
{
	Arrangement arr;
	CGAL::insert(arr, poly.curves_begin(), poly.curves_end());

	// 커브들이 주어진 점들과 만나는 <커브, 점> 셋트 찾는다
	std::list<Query_result> results;
	locate(arr, points.begin(), points.end(), std::back_inserter(results));

	// 검색된 커브를 점으로 쪼갠다
	std::list<Query_result>::const_iterator it;
	for (it = results.begin(); it != results.end(); ++it) {
		if (const Halfedge_const_handle* e =
			boost::get<Halfedge_const_handle>(&(it->second))) // on an edge
		{
			Halfedge_handle eh = arr.non_const_handle(*e);
			const X_monotone_curve_2& curve = (*e)->curve();
			X_monotone_curve_2 c1, c2;
			Traits_2::Point_2 pt = it->first;
			curve.split(pt, c1, c2);
			arr.split_edge(eh, c1, c2);
		}
	}

	// 쪼개진 엣지로부터 새로운 폴리곤을 만든다
	Arrangement::Face_const_iterator ff;
	for (ff = arr.faces_begin(); ff != arr.faces_end(); ++ff)
	{
		Arrangement::Face face = *ff;
		std::list<X_monotone_curve_2> new_poly;
		if (face.has_outer_ccb())
		{
			Arrangement::Ccb_halfedge_const_circulator curr = ff->outer_ccb();
			X_monotone_curve_2 curve = curr->curve();
			new_poly.push_back(curve);
			Traits_2::Point_2 ss = curve.source();
			Traits_2::Point_2 tt = curve.target();
			curr++;

			while (curr != ff->outer_ccb()) {
				curve = curr->curve();

				// 이전 끝점이 지금 커브의 시작점이면
				if (tt == curve.source())
					new_poly.push_back(curve);

				// 지금커브의 끝점이 이전커브의 시작점이면
				else
					new_poly.push_front(curve);

				ss = curve.source();
				tt = curve.target();
				++curr;
			}

			poly.clear();
			poly.init(new_poly.begin(), new_poly.end());
			return;
		}
	}
}

//-----------------------------------------------------------------------------
void SimpleGeometry::segmentation(float64 based_length, float64 based_angle)
{
	Polygons::const_iterator it;
	for (it = polygons_.begin(); it != polygons_.end(); ++it) {
		const Polygon_2& poly = it->first;
		Polygon_2 seg_poly;
		segment_polygon(poly, seg_poly, based_length, based_angle);
		seg_polygons_.push_back(Polygon_tag(seg_poly, it->second));
	}

	for (it = holes_.begin(); it != holes_.end(); ++it) {
		const Polygon_2& poly = it->first;
		Polygon_2 seg_poly;
		//poly.reverse_orientation();
		segment_polygon(poly, seg_poly, based_length, based_angle);
		seg_holes_.push_back(Polygon_tag(seg_poly, it->second));
	}
}

//----------------------------------------------------------------------------
// 참고) BC에서 참조하는 X_monoton_curve_2 는 세그멘테이션 이전임
void SimpleGeometry::linkBC(std::list<const BCNode*>* bc_nodes)
{
	ref_bcs_ = bc_nodes;
}

//----------------------------------------------------------------------------
void SimpleGeometry::segment_polygon(const Polygon_2& source, Polygon_2& result, float64 based_length, float64 based_angle)
{
	Polygon_2::Curve_const_iterator cit;
	for (cit = source.curves_begin(); cit != source.curves_end(); ++cit)
	{
		const X_monotone_curve_2& edge = *cit;

		// 경계커브 테스트는 반드시 세그멘테이션 이전 정보를 활용해야 함!
		bool boundary = is_boundary_curve(edge);
		std::list<X_monotone_curve_2> seg_edges;
		segment_curve(edge, seg_edges, based_length, based_angle, boundary);
		result.insert(seg_edges.begin(), seg_edges.end());
	}
}

//----------------------------------------------------------------------------
void SimpleGeometry::segment_curve(const X_monotone_curve_2& edge, std::list<X_monotone_curve_2>& result, float64 based_length, float64 based_angle, bool boundary)
{
	// 커브의 시작점 끝점
	const Traits_2::Point_2& ss = edge.source();
	const Traits_2::Point_2& tt = edge.target();

	/*
	Traits_2::Point_2 ss, tt;
	if (!fit_based_node(sss, ss)) {
		ss = sss;
	}
	if (!fit_based_node(ttt, tt)) {
		tt = ttt;
	}
	*/

	float64 ssx = CGAL::to_double(ss.x());
	float64 ssy = CGAL::to_double(ss.y());
	float64 ttx = CGAL::to_double(tt.x());
	float64 tty = CGAL::to_double(tt.y());

	// 곡선이면, 무조건 세그멘테이션
	if (edge.is_circular())
	{
		// 커브의 서포팅 원
		const Circle_2& circle = edge.supporting_circle();
		Point_2 cc = circle.center();
		float64 ccx = CGAL::to_double(cc.x());
		float64 ccy = CGAL::to_double(cc.y());
		float64 radii = std::sqrt(CGAL::to_double(circle.squared_radius()));

		float64 sAngle = std::atan2(ssy - ccy, ssx - ccx);
		float64 eAngle = std::atan2(tty - ccy, ttx - ccx);

		float64 dAngle = eAngle - sAngle;
		if (edge.orientation() == CGAL::COUNTERCLOCKWISE) {
			if (dAngle < 0) dAngle += (2 * CGAL_PI);
		}
		else {
			if (dAngle > 0) dAngle -= (2 * CGAL_PI);
		}

		// 세그먼트수는 아크를 10도간격으로 잘랐을때의 세그먼트 숫자와
		// 주어진 길이로 잘랐을때의 세그먼트의 숫자 중
		// 큰 것을 이용한다
		float64 arc_length = radii * abs(dAngle);

		// 경계조건 커브의 경우 기준 길이의 두배정도로 자른다
		if (boundary) {
			based_length = based_length * 2;
			based_angle = based_angle * 2;
		}

		int num_seg1 = int(arc_length / based_length + 0.5);
		if (num_seg1 == 0) num_seg1 = 1;
		int num_seg2 = int(abs(dAngle)*180.0 / CGAL_PI / based_angle + 0.5);

		// 세그먼트 수
		int num_seg = std::max(num_seg1, num_seg2);

		// 세그먼트 수를 짝수개로 조정한다
		if ((num_seg % 2) == 1) num_seg = num_seg + 1;

		// 세그먼트 각도 ; 1도 근처가 될 것임
		float64 delAngle = dAngle / num_seg;


		// 세그멘테이션의 기저 절점
		std::vector<float64> seg_xs(num_seg), seg_ys(num_seg);

		// 시작점
		seg_xs[0] = ssx;
		seg_ys[0] = ssy;

		// 세그멘테이션 절점
		for (int i = 1; i < num_seg; ++i) {
			// 정방향 (절반값)
			float64 p1x = ccx + radii * cos(sAngle + i * delAngle) * 0.5;
			float64 p1y = ccy + radii * sin(sAngle + i * delAngle) * 0.5;

			// 역방향 (절반값)
			float64 p2x = ccx + radii * cos(eAngle - i * delAngle) * 0.5;
			float64 p2y = ccy + radii * sin(eAngle - i * delAngle) * 0.5;


			seg_xs[i] = seg_xs[i] + p1x;
			seg_ys[i] = seg_ys[i] + p1y;
			seg_xs[num_seg - i] = seg_xs[num_seg - i] + p2x;
			seg_ys[num_seg - i] = seg_ys[num_seg - i] + p2y;

		}

		// 끝점
		seg_xs[num_seg] = ttx;
		seg_ys[num_seg] = tty;

		// 실제 커브를 만든다
		for (int i = 0; i < num_seg; ++i) {
			Point_2 p1(seg_xs[i],   seg_ys[i]);
			Point_2 p2(seg_xs[i+1], seg_ys[i+1]);
			X_monotone_curve_2 segmented_edge(p1, p2);
			result.emplace_back(segmented_edge);
		}
	}

	// 직선인 경우
	//boundary edge 이면 세그멘테이션 하고, 그렇지 않으면 세그멘테이션 하지 않는다.
	else 
	{
		if (boundary) 
		{
			// 벡터 길이
			float64 line_length = sqrt((ssx - ttx)*(ssx - ttx) + (ssy - tty)*(ssy - tty));
			int num_seg = (int)(line_length / (2.0*based_length));
			if (num_seg < 1) num_seg = 1;

			// 짝수로 만든다
			if ((num_seg % 2) == 1) num_seg = num_seg + 1;

			// 세그멘테이션의 기저 절점
			std::vector<float64> seg_xs(num_seg), seg_ys(num_seg);

			// 시작점
			seg_xs[0] = ssx;
			seg_ys[0] = ssy;

			// 라인을 세그멘테이션 한다
			for (int i = 1; i < num_seg; ++i) {
				seg_xs[i] = ssx + (ttx - ssx)*((float64)i / (float64)num_seg);
				seg_ys[i] = ssy + (tty - ssy)*((float64)i / (float64)num_seg);
			}

			// 끝점
			seg_xs[num_seg] = ttx;
			seg_ys[num_seg] = tty;

			// 실제 커브를 만든다
			for (int i = 0; i < num_seg; ++i) {
				Point_2 p1(seg_xs[i], seg_ys[i]);
				Point_2 p2(seg_xs[i + 1], seg_ys[i + 1]);
				X_monotone_curve_2 segmented_edge(p1, p2);
				result.emplace_back(segmented_edge);
			}

		} 
		else {
			X_monotone_curve_2 segmented_edge(Point_2(ssx, ssy), Point_2(ttx, tty));
			result.emplace_back(segmented_edge);
		}
	}
}

//-----------------------------------------------------------------------------
bool SimpleGeometry::is_boundary_curve(const X_monotone_curve_2& edge) const
{
	if (ref_bcs_ == nullptr) return false;

	std::list<const BCNode*>::iterator it;
	for (it = ref_bcs_->begin(); it != ref_bcs_->end(); ++it)
	{
		const BCNode* bc = *it;
		if (bc->testCurve(edge)) {
			return true;
		}
	}

	return false;
}

/*
//-----------------------------------------------------------------------------
bool SimpleGeometry::fit_based_node(const Traits_2::Point_2& pt, Traits_2::Point_2& new_pt)
{
	if (based_points_ == nullptr) {
		return false;
	}

	float64 ddd = 10000000;
	std::list<Traits_2::Point_2>::iterator it;
	for (it = based_points_->begin(); it != based_points_->end(); ++it)
	{
		Traits_2::Point_2 pp = *it;
		float64 dx = CGAL::to_double(pp.x()) - CGAL::to_double(pt.x());
		float64 dy = CGAL::to_double(pp.y()) - CGAL::to_double(pt.y());
		float64 dd = sqrt(dx * dx + dy * dy);
		if (dd < ddd) {
			new_pt = pp;
			ddd = dd;
		}
	}
	return true;
}
*/

//-----------------------------------------------------------------------------
void SimpleGeometry::print_polygon(const Polygon_2& poly)
{
	std::cout << "Polygon : ";
	Polygon_2::Curve_const_iterator cc;
	for (cc = poly.curves_begin(); cc != poly.curves_end(); ++cc) {
		std::cout << "([" << (*cc).source() << "]-->[" << (*cc).target() << "]),";
	}
	if (poly.orientation() == CGAL::CLOCKWISE) {
		std::cout << "Hole" << std::endl;
	}
	else {
		std::cout << "Boundary" << std::endl;
	}
	
}