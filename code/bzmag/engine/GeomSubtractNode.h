#ifndef BZMAG_ENGINE_GEOMSUBTRACTNODE_H
#define BZMAG_ENGINE_GEOMSUBTRACTNODE_H

/*
Description : Subtract Node for Boolean Operation
Last Update : 2017.09.28
Author : Jaenam Bae (jaenam@dongyang.ac.kr)
*/

#include "GeomBooleanNode.h"


namespace bzmag
{
namespace engine
{
    class GeomSubtractNode : public GeomBooleanNode
    {
    public:
        GeomSubtractNode();
        virtual ~GeomSubtractNode();
        DECLARE_CLASS(GeomSubtractNode, GeomBooleanNode);
        
    public:
        virtual String description() const;

    public:
        static void bindMethod();
        static void bindProperty();

    protected:
        virtual void boolean_operation(Polygon_set_2& polyset);
    };

#include "geomsubtractnode.inl"

}
}

#endif //BZMAG_ENGINE_GEOMSUBTRACTNODE_H