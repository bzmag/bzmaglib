#ifndef BZMAG_ENGINE_GEOMSPILTNODE_H
#define BZMAG_ENGINE_GEOMSPILTNODE_H

/*
Description : Spilt Node
Last Update : 2020.10.14
Author : Jaenam Bae (jaenam@dongyang.ac.kr)
*/

#include "GeomBaseNode.h"
#include "core/enumeration.h"

namespace bzmag
{
namespace engine
{
	class CSNode;
    class Expression;
    class GeomSpiltNode : public GeomBaseNode
    {
	public:
		enum SPILT_PLANE {
			SPILT_ZX = 1, 
			SPILT_YZ,
			INVALID = -1
		} ;

    public:
		GeomSpiltNode();
        virtual ~GeomSpiltNode();
        DECLARE_CLASS(GeomSpiltNode, GeomBaseNode);

		void setPlane(const SPILT_PLANE& plane);
		const SPILT_PLANE& getPlane() const;

		void setOrientation(bool o);
		bool getOrientation() const;

    public:
		// 이하 재정의 되어야 함
		virtual Transformation getMyTransform();
        virtual String description() const;

	protected:
		virtual bool make_geometry(Polygon_set_2& polyset, Curves& curves, Vertices& vertices, Transformation transform);

    public:
        static void bindMethod();
        static void bindProperty();

	private:
		// 어떻한 평면으로 자를것인가? (ZX 평면? YZ 평면?)
		SPILT_PLANE plane_;

		// 자른후 어떠한 면을 남길것인가? 
		// true : 오른손법칙에 따라 Z->X인 경우 y>=0 인 영역, Y->Z인 경우 x>0= 인 영역
		// true : 오른손법칙에 따라 Z->X인 경우 y<=0 인 영역, Y->Z인 경우 x<=0 인 영역
		bool selectd_plane_;

		static float64 INF_;
    };

#include "geomspiltnode.inl"

}
}

#endif //BZMAG_ENGINE_GEOMSPILTNODE_H