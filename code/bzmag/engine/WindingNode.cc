#include "WindingNode.h"
#include "GeomHeadNode.h"
#include "core/simplepropertybinder.h"
#include "core/nodeeventpublisher.h"

using namespace bzmag;
using namespace bzmag::engine;

IMPLEMENT_CLASS(WindingNode, Node);

//----------------------------------------------------------------------------
WindingNode::WindingNode() : a_(1), I_(0)
{

}

//----------------------------------------------------------------------------
WindingNode::~WindingNode()
{

}

//----------------------------------------------------------------------------
void WindingNode::clear()
{
	a_ = 1;
	I_ = 0;
}

//----------------------------------------------------------------------------
void WindingNode::getRelatedHeadNode(std::list<GeomHeadNode*>& list)
{
	// CS 자식 노드들을 모두 업데이트 시켜놓고
	for (NodeIterator n = firstChildNode();
		n != lastChildNode(); ++n)
	{
		Node* nn = *n;
		CoilNode* coil = dynamic_cast<CoilNode*>(nn);
		if (coil) {
			GeomHeadNode* head = coil->getReferenceNode();
			if(head) list.push_back(head);
		}
	}
}

//----------------------------------------------------------------------------
bool WindingNode::update()
{
    return true;
}

//----------------------------------------------------------------------------
void WindingNode::onAttachTo(Node* parent)
{

}


//----------------------------------------------------------------------------
void WindingNode::onDetachFrom(Node* parent)
{

}


//----------------------------------------------------------------------------
void WindingNode::bindProperty()
{
	BIND_PROPERTY(float64, Current, &setCurrent, &getCurrent);
	BIND_PROPERTY(int32, ParallelBranches, &setNumberOfParallelBranches, &getNumberOfParallelBranches);
}

