#ifndef BZMAG_ENGINE_GEOMHEADNODE_H
#define BZMAG_ENGINE_GEOMHEADNODE_H

/*
Description : Head Node for Handling a Geometry Node
Last Update : 2017.09.28
Author : Jaenam Bae (jaenam@dongyang.ac.kr)
*/

#include "GeomBaseNode.h"
#include "core/color.h"

namespace bzmag
{
namespace engine
{
	class MaterialNode;
    class GeomHeadNode : public GeomBaseNode
    {
    public:
        GeomHeadNode();
        virtual ~GeomHeadNode();
        DECLARE_CLASS(GeomHeadNode, GeomBaseNode);

    public:
        // 컬러설정
        void setColor(const Color& color);
        const Color& getColor() const;

        // Visualization을 하지 않을 것인지?
        void setHideStatus(bool hide);
        bool isHide() const;

        // Boolean 연산에 의해 참조되는 않는 노드인지?
        void setStandAlone(bool standalone);
        bool isStandAlone() const;

		// Z-Order 설정용
		bool contain(GeomHeadNode* node);

        // 해석에 쓰일 모델 노드인지?
        void setModelNode(bool model);
        bool isModelNode() const;

        // Head노드가 참조하는 가장 말단 자식 노드
        void setLastNode(GeomBaseNode* last);
		GeomBaseNode* getLastNode() const;

        // 요소생성 갯수 (원하는 값)
        void setNumberOfElements(int32 ne);
        int32 getNumberOfElements() const;

        // 재질 설정 
        void setMaterialNode(Node* material);
        Node* getMaterialNode() const;

    public:
		// 이하 재정의 되어야 함
		virtual bool makeGeometry(Transformation trans = Transformation());
		virtual Transformation getMyTransform();
        virtual String description() const;
		
    public:
		virtual bool update();
        virtual void onAttachTo(Node* parent);
        virtual void onDetachFrom(Node* parent);

    public:
        static void bindMethod();
        static void bindProperty();

	protected:
		virtual bool make_geometry(Polygon_set_2& polyset, Curves& curves, Vertices& vertices, Transformation transform);
		virtual void updateTransform();
		virtual void updateLinkedNode();

    protected:
        bool bStandalone_;
        bool bModelNode_;
		bool bHide_;

        Color color_;
        int32 num_elements_;

		// 최종 연산된 Geometry 노드
        GeomBaseNode* lastNode_;

		// 참조 재질
        Ref<MaterialNode> material_;
    };

#include "geomheadnode.inl"

}
}

#endif //BZMAG_ENGINE_GEOMHEADNODE_H