#include "core/define.h"
#include "core/unittest.h"
#include "core/kernel.h"
#include "core/module.h"
#include "bzmagpylib/pythonscriptserver.h"

using namespace bzmag;

INCLUDE_MODULE(Engine);
INCLUDE_MODULE(bzmagPy);

//-----------------------------------------------------------------------------
int main(int argc, char* argv[])
{
    // initialize DynamicModules    
    USING_MODULE(Engine);
    USING_MODULE(bzmagPy);

    bzmagPyScriptServer* pyServer = bzmagPyScriptServer::instance();

    String res;
    pyServer->run(String("import bzmagPy as bz"), &res);

    printf("%s", res.c_str());
    return 0;
}