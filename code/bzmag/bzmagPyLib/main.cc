#include <python.h>
#include "core/define.h"
#include "core/unittest.h"
#include "core/kernel.h"
#include "core/module.h"
#include "pythonscriptserver.h"

using namespace bzmag;

//-----------------------------------------------------------------------------
PyObject* g_module = 0;
//bzNodes g_bzobjects;
bool bzmagPyScriptServer::s_standAlone_ = false;
bzmagPyScriptServer::NodeEventSubscriberImpl
bzmagPyScriptServer::nodeEventSubscriber_;


//-----------------------------------------------------------------------------
void initialize_bzmagPy(Module* module)
{
    REGISTER_TYPE(module, bzmagPyScriptServer);
    bzmagPyScriptServer::setSingletonPath("/sys/server/script/python");
}


//-----------------------------------------------------------------------------
void finalize_bzmagPy(Module* module)
{
    if (bzmagPyScriptServer::s_standAlone_) {
        Py_Finalize();
    }
}

//-----------------------------------------------------------------------------
DECLARE_MODULE(bzmagPy);

