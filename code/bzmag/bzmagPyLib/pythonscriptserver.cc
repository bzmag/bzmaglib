#include "python_extension.h"
#include "pythonscriptserver.h"
#include "core/kernel.h"
#include "core/module.h"
#include "core/nodeeventpublisher.h"

using namespace bzmag;

IMPLEMENT_CLASS(bzmagPyScriptServer, ScriptServer);

//-----------------------------------------------------------------------------
static PyMethodDef bzmagPyMethods[] =
{
    { "newobj",  bzmagPy_newobject, METH_VARARGS, "create new nonamed bzObject" },
    { "new",  bzmagPy_new, METH_VARARGS, "create new bzNode" },
    { "get",  bzmagPy_get, METH_VARARGS, "get TobObject from NOH" },
	{ "getObject",  bzmagPy_getObjectByID, METH_VARARGS, "get Object by ID" },
	{ "getNode",  bzmagPy_getNodeByID, METH_VARARGS, "get Node by ID" },
    { "delete",  bzmagPy_delete, METH_VARARGS, "delete bzNode" },
    { "ls",  bzmagPy_ls, METH_VARARGS, "list current work node" },
    { "pushcwn",  bzmagPy_pushCwn, METH_VARARGS, "push current work node" },
    { "popcwn",  bzmagPy_popCwn, METH_VARARGS, "pop current work node" },
    { "print",  bzmagPy_print, METH_VARARGS, "debug print" },
    { "exit",  bzmagPy_exit, METH_VARARGS, "exit application" },
//     { "serialize",  bzmagPy_serialize, METH_VARARGS, "serialize objects to resource" },
//     { "deserialize",  bzmagPy_deserialize, METH_VARARGS, "deserialize objects from resource" },
    { "getModuleList",  bzmagPy_getModuleList, METH_VARARGS, "get module list" },
    { "getTypeList",  bzmagPy_getTypeList, METH_VARARGS, "get type list specified module name" },
    { "getDerivedTypes",  bzmagPy_getDerivedTypes, METH_VARARGS, "get derived type list specified type name" },
    { "getTypeInfo",  bzmagPy_getTypeInfo, METH_VARARGS, "get type information specified type name" },
    { "isNode",  bzmagPy_isNode, METH_VARARGS, "specify the given type is kind of bzNodes" },
    { NULL, NULL, 0, NULL }        /* Sentinel */
};

//-----------------------------------------------------------------------------
static struct PyModuleDef moduledef = {
    PyModuleDef_HEAD_INIT,
    "bzmagPy",       /* m_name */
    "bzmagPy module",/* m_doc */
    -1,              /* m_size */
    bzmagPyMethods,  /* m_methods */
    NULL,            /* m_reload */
    NULL,            /* m_traverse */
    NULL,            /* m_clear */
    NULL             /* m_free */
};


//-----------------------------------------------------------------------------
 static void at_Exit()
 {
     // if s_standAlone_ == true then this function called at exit Python
     // SingletonServer::clear() must be called by current process(Python).
     SingletonServer::instance()->clear();
 }


//-----------------------------------------------------------------------------
unsigned long long bzmagPyScriptServer::initialize()
{
    // initialize bzmagPy module
    PyStatus status;
    PyConfig config;
    PyConfig_InitPythonConfig(&config);

    status = PyConfig_SetString(&config, &config.program_name, L"bzmagPy");
    if (PyStatus_Exception(status)) {
        goto exception;
    }
    g_module = PyModule_Create(&moduledef);

    // 예외 객체 초기화
    PyObject* error_obj = PyErr_NewException("bzmagPy.error", NULL, NULL);
    if (error_obj == NULL) {
        return NULL;  // 예외 객체 생성 실패 시, 초기화 실패 처리
    }
    Py_INCREF(error_obj);
    if (PyModule_AddObject(g_module, "error", error_obj) < 0) {
        Py_DECREF(error_obj);  // 실패 시 메모리 해제
        return NULL;
    }

    // bzObject 타입 초기화
    if (PyType_Ready(&bzObjectType) < 0) {
        return NULL;  // 타입 준비 실패 시 초기화 실패 처리
    }
    Py_INCREF(&bzObjectType);
    if (PyModule_AddObject(g_module, "bzObject", (PyObject*)&bzObjectType) < 0) {
        Py_DECREF(&bzObjectType);  // 실패 시 메모리 해제
        return NULL;
    }

    // bzNode 타입 초기화
    if (PyType_Ready(&bzNodeType) < 0) {
        return NULL;  // 타입 준비 실패 시 초기화 실패 처리
    }
    Py_INCREF(&bzNodeType);
    if (PyModule_AddObject(g_module, "bzNode", (PyObject*)&bzNodeType) < 0) {
        Py_DECREF(&bzNodeType);  // 실패 시 메모리 해제
        return NULL;
    }

     //if (nullptr == Kernel::instance()->lookup("/sys/server/script/python"))
     //{
     //    bzmagPyScriptServer::setSingletonPath("/sys/server/script/python");
     //    bzmagPyScriptServer::s_standAlone_ = true;
     //    Py_AtExit(at_Exit);
     //}
     //else
     //{
     //    bzmagPyScriptServer::setSingletonPath("/sys/server/script/python");
     //    bzmagPyScriptServer::s_standAlone_ = false;
     //}
     //bzmagPyScriptServer::setSingletonPath("/sys/server/script/python");
    return (unsigned long long)g_module;

exception:
    PyConfig_Clear(&config);
    Py_ExitStatusException(status);
}


//-----------------------------------------------------------------------------
void bzmagPyScriptServer::NodeEventSubscriberImpl::onAttachTo
(Node* parent, Node* child)
{
}

//-----------------------------------------------------------------------------
void bzmagPyScriptServer::NodeEventSubscriberImpl::onDetachFrom
(Node* parent, Node* child)
{
}


//-----------------------------------------------------------------------------
bzmagPyScriptServer::bzmagPyScriptServer()
{
    // Test to see if we are running inside an existing Python interpreter
    if (!Py_IsInitialized())
    {
        Py_Initialize();
        initialize();
    }
}


//-----------------------------------------------------------------------------
bzmagPyScriptServer::~bzmagPyScriptServer()
{

}


//-----------------------------------------------------------------------------
bool bzmagPyScriptServer::run(const String& str, String* result)
{
    if (PyRun_SimpleString(str) == -1)
        return false;
    return true;
}


//-----------------------------------------------------------------------------
bool bzmagPyScriptServer::call(const String& str, Parameter* parameter)
{
    return false;
}


//-----------------------------------------------------------------------------
bool bzmagPyScriptServer::runFile(const Uri& uri, String* result)
{
    return false;
}


