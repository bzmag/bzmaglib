#include <python.h>
#include "core/define.h"
#include "core/kernel.h"
#include "core/module.h"
#include "bzmagpylib/pythonscriptserver.h"

using namespace bzmag;

INCLUDE_MODULE(Engine);
INCLUDE_MODULE(bzmagPy);

//-----------------------------------------------------------------------------
extern PyObject* g_module;

PyMODINIT_FUNC PyInit_bzmagPy(void)
{
    // initialize DynamicModules    
    USING_MODULE(Engine);
    USING_MODULE(bzmagPy);

    bzmagPyScriptServer::initialize();

    return g_module;
}


